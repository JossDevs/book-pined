<?php

namespace Tests\Feature\Exam;

use App\ActiviyQuestion;
use App\Exam;
use App\NewActivitie;
use App\QuestionDetail;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateExamTest extends TestCase
{
    use RefreshDatabase;
    public function createExam(NewActivitie $newActivitie, $data =[])
    {
        $url = route('books.exam.store', $newActivitie); 
        return $this->actingAsRandomUser()->post($url, $data);
    }
    /**
     * @test
    */
    public function can_create_exam_student()
    {
        $this->withoutExceptionHandling();
        //Arrange
        $newActivitie = factory(NewActivitie::class)->create();
        $question = factory(ActiviyQuestion::class)->create([
            'new_activitie_id' => $newActivitie->id
        ]);
        $questionDetail = factory(QuestionDetail::class)->create([
            'activiy_question_id' => $question->id            
        ]);
        $examDetail = factory(Exam::class)->make([
            'question_detail_id' => $questionDetail->id
        ]);
       // dd($examDetail);

        //Act
        $response = $this->createExam($newActivitie,[
            $questionDetail->id => $examDetail->detail
        ]);

        //assert
        $response->assertOk();
        

    }
}
