<?php

namespace Tests\Feature\Exam;

use App\ActiviyQuestion;
use App\NewActivitie;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShowExamTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * @test
     */

    public function can_see_exm()
    {
        // Arrange      
        $newActivitie = factory(NewActivitie::class)->create();
        factory(ActiviyQuestion::class)->create([
            'new_activitie_id' => $newActivitie->id
        ]);
        $url = route('books.exam.show', $newActivitie); 

        // Act
        $response = $this->actingAsRandomUser()->get($url);

        // Assert
        $response->assertOk();

        $response->assertViewIs('books.modules.exam.show');

    }
}
