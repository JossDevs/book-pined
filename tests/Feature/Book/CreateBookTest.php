<?php

namespace Tests\Feature\Book;

use App\Book;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class CreateBookTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        Storage::fake('books');
    }

    private function createBook($data = [])
    {
        $url = route('books.store');

        return $this->actingAsRandomUser()->post($url, $data);
    }

    /**
     * @test
     */
    public function can_see_create_book_form()
    {
        // Arrange
        $url = route('books.create');

        // Act
        $response = $this->actingAsRandomUser()->get($url);

        // Assert
        $response->assertOk();

        $response->assertViewIs('books.create');
    }

    /**
     * @test
     */
    public function can_create_book()
    {
        // Arrange
        $data = factory(Book::class)->make();

        // Act
        $response = $this->createBook([
            'name' => $data->name,
            'cover' => UploadedFile::fake()->image('cover.jpg')
        ]);

        // Assert
        $response->assertRedirect(route('books.index'));

        $response->assertSessionHas('flash_success', 'Libro creado con éxito.');

        $this->assertEquals(1, Book::count());

        $book = Book::first();

        $this->assertEquals($data->name, $book->name);
    }

    /**
     * @test
     */
    public function fields_are_required()
    {
        // Arrange
        $data = factory(Book::class)->make();

        // Act
        $response = $this->createBook([
            'name' => null,
            'cover' => null
        ]);

        // Assert
        $response->assertSessionHasErrors([
            'name',
            'cover'
        ]);
    }
}
