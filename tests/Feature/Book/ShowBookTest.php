<?php

namespace Tests\Feature\Book;

use App\Book;
use App\Module;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShowBookTest extends TestCase
{
    use RefreshDatabase;

    private function showBook(Book $book)
    {
        $url  = route('books.show', $book);

        return $this->actingAsRandomUser()->get($url);
    }

    /**
     * @test
     */
    public function can_see_details_book()
    {
        // Arrange
        $book = factory(Book::class)->create();

        factory(Module::class, 10)->create([
            'book_id' => $book->id
        ]);

        // Act
        $response = $this->showBook($book);

        // Assert
        $response->assertOk();

        $response->assertViewIs('books.show');

        $this->assertCount(10, $response->original->book->modules);
    }
}
