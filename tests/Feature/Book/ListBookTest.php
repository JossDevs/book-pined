<?php

namespace Tests\Feature\Book;

use App\Book;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ListBookTest extends TestCase
{
    use RefreshDatabase;

    private function listBook()
    {
        $url  = route('books.index');

        return $this->actingAsRandomUser()->get($url);
    }

    /**
     * @test
     */
    public function can_see_list_books()
    {
        // Arrange
        factory(Book::class, 10)->create();

        // Act
        $response = $this->listBook();

        // Assert
        $response->assertOk();

        $response->assertViewIs('books.index');

        $response->assertViewHas('books');

        $this->assertCount(10, $response->original->books);
    }
}
