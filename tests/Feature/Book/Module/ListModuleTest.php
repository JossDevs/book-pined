<?php

namespace Tests\Feature\Book\Module;

use App\Book;
use App\Module;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ListModuleTest extends TestCase
{
    use RefreshDatabase;

    private function listModule(Book $book)
    {
        $url = route('books.modules.index', $book);

        return $this->actingAsRandomUser()->get($url);
    }

    /**
     * @test
     */
    public function can_see_a_module()
    {
        // Arrange
        $book = factory(Book::class)->create();

        factory(Module::class, 10)->create([
            'book_id' => $book->id
        ]);

        // Act
        $response = $this->listModule($book);

        // Assert
        $response->assertOk();

        $response->assertViewIs('books.modules.index');

        $this->assertCount(10, $response->original->book->modules);
    }

    /**
     * @test
     */
    public function cannot_see_modules_from_another_book()
    {
        // Arrange
        $book = factory(Book::class)->create();

        factory(Module::class, 10)->create();

        // Act
        $response = $this->listModule($book);

        // Assert
        $response->assertOk();

        $response->assertViewIs('books.modules.index');

        $this->assertCount(0, $response->original->book->modules);
    }
}
