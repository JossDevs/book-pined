<?php

namespace Tests\Feature\Livewire\Modules;

use App\Http\Livewire\ModuleImages;
use App\Module;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Livewire\Livewire;
use Livewire\Testing\TestableLivewire;
use Tests\TestCase;

class ListModuleImagesTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        Storage::fake('books');
    }

    private function buildComponent(Module $module): TestableLivewire
    {
        return Livewire::test(ModuleImages::class, [
            'moduleId' => $module->id
        ]);
    }

    /**
     * @test
     */
     public function can_create_component()
     {
        //  Arrange
        $module = factory(Module::class)->create();

        // Act
        $component = $this->buildComponent($module);

        // Assert
        $component->assertStatus(200);
     }

    /**
     * @test
     */
     public function can_go_to_next_image()
     {
        //  Arrange
        $module = factory(Module::class)->create();

        $imageOne = UploadedFile::fake()->image('portada.jpg');
        $imageTwo = UploadedFile::fake()->image('portada.jpg');

        $module->addMedia($imageOne)->toMediaCollection('images');
        $module->addMedia($imageTwo)->toMediaCollection('images');

        $this->assertCount(2, $module->images());

        $component = $this->buildComponent($module);

        $component->assertSee($module->images()->get(0)->present()->getUrl());
        $component->assertDontSee($module->images()->get(1)->present()->getUrl());

        // Act
        $component->call('nextImage');

        // Assert
        $component->assertSee($module->images()->get(1)->present()->getUrl());
        $component->assertDontSee($module->images()->get(0)->present()->getUrl());
     }

    /**
     * @test
     */
     public function can_go_to_previous_image()
     {
        //  Arrange
        $module = factory(Module::class)->create();

        $imageOne = UploadedFile::fake()->image('portada.jpg');
        $imageTwo = UploadedFile::fake()->image('portada.jpg');

        $module->addMedia($imageOne)->toMediaCollection('images');
        $module->addMedia($imageTwo)->toMediaCollection('images');

        $this->assertCount(2, $module->images());

        $component = $this->buildComponent($module);

        $component->call('nextImage');

        $component->assertSee($module->images()->get(1)->present()->getUrl());
        $component->assertDontSee($module->images()->get(0)->present()->getUrl());

        // Act
        $component->call('previousImage');

        // Assert
        $component->assertSee($module->images()->get(0)->present()->getUrl());
        $component->assertDontSee($module->images()->get(1)->present()->getUrl());
     }
}
