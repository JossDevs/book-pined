<?php

namespace Tests\Feature\Modules;

use App\Book;
use App\Module;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class CreateModuleTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        Storage::fake('books');
    }

    private function createModule(Book $book, $data)
    {
        $url = route('books.modules.store', $book);

        return $this->actingAsRandomUser()->post($url, $data);
    }

    /**
     * @test
     */
    public function can_see_create_module_form()
    {
        // Arrange
        $book = factory(Book::class)->create();

        $url = route('books.modules.create', $book);

        // Act
        $response = $this->actingAsRandomUser()->get($url);

        // Arrange
        $response->assertOk();

        $response->assertViewIs('books.modules.create');

        $response->assertViewHas('book');

    }

    /**
     * @test
     */
    public function can_create_a_module_with_html()
    {
        // Arrange
        $book = factory(Book::class)->create();

        $data = factory(Module::class)->make();
        $this->withoutExceptionHandling();
        $this->assertEquals(0, Module::count());

        // Act
        $response = $this->createModule($book, [
            'name' => $data->name,
            'type' => 'html',
            'body' => $data->body
        ]);

        // Assert
        $response->assertRedirect(
            route('books.show', $book)
        );

        $response->assertSessionHas('flash_success', 'Módulo creado con éxito.');

        $this->assertEquals(1, Module::count());

        $module = Module::first();
        $this->assertEquals($data->name, $module->name);
        $this->assertEquals($data->body, $module->body);
    }

    /**
     * @test
     */
    public function can_create_module_with_images()
    {
        // Arrange
        $book = factory(Book::class)->create();

        $data = factory(Module::class)->make();

        $this->assertCount(0, $book->modules);

        $images = [
            UploadedFile::fake()->image('portada.jpg'),
            UploadedFile::fake()->image('inicio.jpg'),
            UploadedFile::fake()->image('final.jpg')
        ];

        // Act
        $response = $this->createModule($book, [
            'name' => $data->name,
            'file' => $images,
            'type' => 'images'
        ]);

        // Assert
        $response->assertRedirect(
            route('books.show', $book)
        );

        $response->assertSessionHas('flash_success', 'Módulo creado con éxito.');

        $this->assertEquals(1, Module::count());

        $module = Module::first();
        $this->assertEquals($data->name, $module->name);

        $this->assertCount(3, $module->images());
    }

    /**
     * @test
     */
    public function field_body_is_required_if_file_is_null()
    {
        // Arrange
        $book = factory(Book::class)->create();

        $data = factory(Module::class)->create();

        $images = [
            UploadedFile::fake()->image('portada.jpg'),
            UploadedFile::fake()->image('inicio.jpg'),
            UploadedFile::fake()->image('final.jpg')
        ];

        // Act
        $response = $this->createModule($book, [
            'name' => $data->name,
            'body' => null,
        ]);

        // Assert
        $response->assertSessionHasErrors([
            'body'
        ]);

        $this->assertCount(0, $book->modules);
    }

    /**
     * @test
     */
    public function field_file_is_required_if_body_is_null()
    {
        // Arrange
        $book = factory(Book::class)->create();

        $data = factory(Module::class)->create();

        // Act
        $response = $this->createModule($book, [
            'name' => $data->name,
            'body' => null,
            'file' => null,
        ]);

        // Assert
        $response->assertSessionHasErrors([
            'file'
        ]);

        $this->assertCount(0, $book->modules);
    }
}
