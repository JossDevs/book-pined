<?php

namespace Tests\Feature\Books\Assignment;

use App\Book;
use App\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateBooksAssignmentTest extends TestCase
{
    use RefreshDatabase;

    private function createBooksAssignment(Book $book, Student $student)
    {
        $url = route('books.assignment.store', [$book, $student]);

        return $this->actingAsRandomUser()->post($url);
    }

    /**
     * @test
     */
    public function can_see_list_students()
    {
        // Arrange
        factory(Student::class)->create();

        $book = factory(Book::class)->create();

        $url = route('books.assignment.show', $book);

        // Act
        $response = $this->actingAsRandomUser()->get($url);

        // Assert
        $response->assertOk();

        $response->assertViewIs('books.assignment.show');

        $response->assertViewHas([
            'book',
            'students'
        ]);
    }

    /**
     * @test
     */
    public function can_assign_book_to_a_student()
    {
        // Arrange
        $student = factory(Student::class)->create();

        $book = factory(Book::class)->create();

        $this->assertCount(0, $student->books);

        // Act
        $response = $this->createBooksAssignment($book, $student);

        // Assert
        $response->assertRedirect(route('books.assignment.show', $book));

        $response->assertSessionHas('flash_success', 'Libro asignado con éxito.');

        $this->assertCount(1, $student->fresh()->books);
    }

    /**
     * @test
     */
    public function can_remove_book_to_a_student()
    {
        // Arrange
        $student = factory(Student::class)->create();

        $book = factory(Book::class)->create();

        $student->books()->attach([$book->id]);

        $this->assertCount(1, $student->books);

        $url = route('books.assignment.destroy', [$book, $student]);

        // Act
        $response = $this->actingAsRandomUser()->delete($url);

        // Assert
        $response->assertRedirect(route('books.assignment.show', $book));

        $response->assertSessionHas('flash_success', 'Se removio el libro con éxito.');

        $this->assertCount(0, $student->fresh()->books);
    }
}
