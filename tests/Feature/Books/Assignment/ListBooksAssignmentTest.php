<?php

namespace Tests\Feature\Books\Assignment;

use App\Book;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ListBooksAssignmentTest extends TestCase
{
    use RefreshDatabase;

    private function listBooksAssignment()
    {
        $url = route('books.assignment.index');

        return $this->actingAsRandomUser()->get($url);
    }

    /**
     * @test
     */
    public function can_see_books_list()
    {
        // Arrange
        $books = factory(Book::class, 10)->create();

        // Act
        $response = $this->listBooksAssignment();

        // Assert
        $response->assertOk();

        $response->assertViewIs('books.assignment.index');

        $response->assertViewHas('books');

        $this->assertCount(10, $response->original->books);
    }
}
