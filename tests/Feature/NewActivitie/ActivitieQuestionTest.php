<?php

namespace Tests\Feature\NewActivitie;

use App\ActiviyQuestion;
use App\Module;
use App\NewActivitie;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ActivitieQuestionTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * @test
     */

    public function can_see_list_activitie_question()
    {
       $this->withoutExceptionHandling();
        // Arrange      
        $module = factory(Module::class)->create();
        
        $activitie = factory(NewActivitie::class)->create([
            'module_id' => $module->id
        ]);

        $activitieQuestions = factory(ActiviyQuestion::class)->times(2)->create([
            'new_activitie_id' => $activitie->id
        ]);

        $url = route('books.question.index', $activitie); 

        // Act
        $response = $this->actingAsRandomUser()->get($url);

        // Assert
        $response->assertOk();
        
        $response->assertViewIs('books.modules.new-activities.questions.index');

        $response->assertViewHas('questions');

        $this->assertCount(2, $response->original->questions);
    }
    /**
     * @test
     */
    public function can_see_activitie_question_store()
    {  $this->withoutExceptionHandling();
        //Arrange

        $activitie = factory(NewActivitie::class)->create();
        $data = factory(ActiviyQuestion::class)->make([
            'new_activitie_id' => $activitie->id
        ]);

        $url = route('books.question.store', $activitie);
        
        //Act

        $response = $this->actingAsRandomUser()->post($url, [
        'name'              =>$data->name,
        'input_type'         =>$data->input_type,
        'percentage'        =>$data->percentage,
        ]);

        //Assert
        $response->assertRedirect(route('books.newActivitie.show', $activitie));

        
    }
    /**
     * @test
     */
    public function can_see_activitie_question_form()
    {  $this->withoutExceptionHandling();
        //Arrange

        $activitie = factory(NewActivitie::class)->create();
        

        $url = route('books.question.create', $activitie);
        
        //Act

        $response = $this->actingAsRandomUser()->get($url);

        //Assert
        $response->assertViewIs('books.modules.new-activities.questions.create');

        $response->assertViewHas('activitie');

        
    }
    /**
     * @test
     */
    public function can_see_activitie_question_delete()
    { 
        $this->withoutExceptionHandling();
        //Arrange
        $activitie = factory(NewActivitie::class)->create();
        $data = factory(ActiviyQuestion::class)->create([
            'new_activitie_id' => $activitie->id
        ]);
     
        $url = route('books.question.destroy', $data);
        //Act
        $response =  $this->actingAsRandomUser()->delete($url);

        //Assert
        $question = ActiviyQuestion::all()->count();

        //dd($question);
        $this->assertEquals(0,$question);
        $response->assertRedirect(route('books.newActivitie.show',$activitie));

    }
    /**
     * @test
     */
    public function can_see_activitie_question_edit()
    { $this->withoutExceptionHandling();
        //Arrange
        $activitie = factory(NewActivitie::class)->create();
        $question = factory(ActiviyQuestion::class)->create([
            'new_activitie_id' =>$activitie->id
        ]);
        $data = factory(ActiviyQuestion::class)->make();
     
        $url = route('books.question.update', $question);
        //Act
        $response =  $this->actingAsRandomUser()->post($url,[
        'name' => $data->name,
        'input_type' =>$data->input_type,
        'percentage' =>$data->percentage,
        ]);

        //Assert
        $response->assertRedirect(route('books.newActivitie.show', $response->original->newActivitie));

    }
}
