<?php

namespace Tests\Feature\NewActivitie;

use App\Book;
use App\Module;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\NewActivitie;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NewActivitieTest extends TestCase
{
    use DatabaseTransactions;

    private function List_new_activitie( Module $module)
    {
        $url = route('books.newActivitie.index', $module);
        
        return $this->actingAsRandomUser()->get($url);
        //return $this->actingAsAdmin()->get($url);
    }
    /**
     * @test
     */
    public function can_see_list_new_activitie()
    {
        // Arrange
        $module = factory(Module::class)->create();
        
        $newActivitie = factory(NewActivitie::class)->create([
            'module_id' => $module->id
        ]);

        // Act
        $response = $this->List_new_activitie($module);

        // Assert
        $response->assertOk();
        
        $response->assertViewIs('books.modules.new-activities.index');

        $response->assertViewHas('newActivities');

        $this->assertCount(1, $response->original->newActivities);
    }
    /**
     * @test
     */
     public function can_see_new_activitie_form()
    { 
        //Arrange
        $module = factory(Module::class)->create();
        $url = route('books.newActivitie.create', $module);        

        //Act
        $response =  $this->actingAsRandomUser()->get($url);

        //Assert
         $response->assertOk();
         $response->assertViewIs('books.modules.new-activities.create');
         $response->assertViewHas('module');

    }
    /**
     * @test
     */
    public function can_see_new_activitie_store()
    { 
        //Arrange
        $module = factory(Module::class)->create();
        $data = factory(NewActivitie::class)->make();
     
        $url = route('books.newActivitie.store', $module);
        //Act
        $response =  $this->actingAsRandomUser()->post($url, [
            'name' => $data->name,
            'module_id'=> $module->id
        ]);
        $response->assertOk();
        $response->assertViewIs('books.modules.new-activities.show');
        $response->assertViewHas(['module', 'book','newActivitie']);

        //Assert

    }
    /**
     * @test
     */
    public function can_see_new_activitie_show()
    { 
        //Arrange
        $data = factory(NewActivitie::class)->create();
     
        $url = route('books.newActivitie.show', $data);
        //Act
        $response =  $this->actingAsRandomUser()->get($url);

        //Assert
        
        $response->assertOk();
        $response->assertViewIs('books.modules.new-activities.show');
        $response->assertViewHas(['module', 'book','newActivitie']);

    }
    /**
     * @test
     */
    public function can_see_new_activitie_delete()
    { 
        $this->withoutExceptionHandling();
        //Arrange
        $data = factory(NewActivitie::class)->create();
     
        $url = route('books.newActivitie.destroy', $data);
        //Act
        $response =  $this->actingAsRandomUser()->delete($url);

        //Assert
        
        $response->assertOk();
        $response->assertViewIs('books.modules.new-activities.create');
        $response->assertViewHas('module');

    }
}
