<?php

namespace Tests\Feature\NewActivitie;

use App\ActiviyQuestion;
use App\QuestionDetail;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class QuestionDetailTest extends TestCase
{
    use DatabaseTransactions;

    private function createDetail(ActiviyQuestion $activitiyQuestions , $data = [])
    {


      
        $url = route('books.detail.store', $activitiyQuestions);

        return $this->actingAsRandomUser()->post($url, $data);
    }

    /**
     * @test
     */
    public function can_see_list_activitie_question_detail()
    {
        // Arrange      
        
        $activitiyQuestions = factory(ActiviyQuestion::class)->create();
        $questionDetail = factory(QuestionDetail::class)->create([
            'activiy_question_id' => $activitiyQuestions->id
        ]);

       
        $url = route('books.detail.index', $activitiyQuestions);        


        // Act
        $response = $this->actingAsRandomUser()->get($url);

        // Assert
        $response->assertOk();
       
        $response->assertViewIs('books.modules.new-activities.questions.question-detail.index');

        $response->assertViewHas('questionDetails');
    }

     /**
     * @test
     */
    public function can_see_store_activitie_question_detail()
    {
        // Arrange              
        $activitiyQuestions = factory(ActiviyQuestion::class)->create();
        $preData = factory(QuestionDetail::class)->make([
            'activiy_question_id' => $activitiyQuestions->id
        ]);

        // Act  
        $response = $this->createDetail($activitiyQuestions, [
            'detail' => $preData->detail,
            'is_correct' => $preData->is_corret,            
        ]);

        // Assert
        $response->assertStatus(302); 

        
    }

    
     /**
     * @test
     */
    public function can_see_delete_activitie_question_detail()
    {
        // Arrange
        $activitiyQuestions = factory(ActiviyQuestion::class)->create();
        $data = factory(QuestionDetail::class)->create([
            'activiy_question_id' => $activitiyQuestions->id
        ]);
        $url = route('books.detail.delete',[$activitiyQuestions, $data]);
        

        // Act  
        
            $response = $this->actingAsRandomUser()->delete($url);

        // Assert
        $detail = QuestionDetail::all()->count();
        $this->assertEquals(0, $detail);      

        
    }
    
     /**
     * @test
     */
    public function can_see_update_activitie_question_detail()
    {
        // Arrange
        $activitiyQuestions = factory(ActiviyQuestion::class)->create();
        $detail = factory(QuestionDetail::class)->create([
            'activiy_question_id' => $activitiyQuestions->id
        ]);
        $data = factory(QuestionDetail::class)->make([
            'activiy_question_id' => $activitiyQuestions->id
        ]);
        $url = route('books.detail.update',[$activitiyQuestions, $detail]); 

        //Act        
        $response = $this->actingAsRandomUser()->post($url, [
            'detail' => $data->detail,
            'invoice_type' => false
        ]);

        // Assert
        $detail->refresh();

        $this->assertEquals($detail->name, $data->name);

        $this->assertEquals($detail->invoice_type, $data->invoice_type);   

        
    }
}
