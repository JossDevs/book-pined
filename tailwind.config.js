const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
    important: true,
    theme: {
        extend: {
            fontFamily: {
                sans: ['Inter var', ...defaultTheme.fontFamily.sans],
            },

            minWidth: {
                '8': '2rem',
                '16': '4rem',
                '32': '8rem',
            }
        }
    },
    variants: {},
    plugins: [
        require('@tailwindcss/ui'),
    ],
}
