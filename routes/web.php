<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {

    Route::prefix('/student')->group(function () {
        Route::get('/', 'StudentController@index')->name('students.index');
        Route::get('/create', 'StudentController@create')->name('students.create');
        Route::post('/store', 'StudentController@store')->name('students.store');
        Route::get('/show/{student}', 'StudentController@show')->name('students.show');
        Route::get('/edit/{student}', 'StudentController@edit')->name('students.edit');
        Route::post('/update', 'StudentController@edit')->name('students.update');
        
        Route::delete('/detroy/{student}', 'StudentController@destroy')->name('students.destroy');

    });

    Route::prefix('/books')->group(function () {

        Route::prefix('/assignment')->group(function () {
            Route::get('/', 'BookAssignmentController@index')->name('books.assignment.index');
            Route::get('/show', 'BookAssignmentController@show')->name('books.assignment.show');
            Route::post('/store/{student}', 'BookAssignmentController@store')->name('books.assignment.store');
            Route::delete('/destroy/{student}', 'BookAssignmentController@destroy')->name('books.assignment.destroy');
        });
        Route::prefix('/new-activitie')->group(function () {
            Route::get('/{module}', 'NewActivitieController@index')->name('books.newActivitie.index');
            Route::get('/{module}/create', 'NewActivitieController@create')->name('books.newActivitie.create');
            Route::get('/{newActivitie}/show', 'NewActivitieController@show')->name('books.newActivitie.show');
            Route::post('/{module}/store', 'NewActivitieController@store')->name('books.newActivitie.store');
            Route::delete('/{newActivitie}/delete', 'NewActivitieController@destroy')->name('books.newActivitie.destroy');
            Route::prefix('/question')->group(function () {
                Route::get('/{newActivitie}', 'ActivityQuestionController@index')->name('books.question.index'); 
                Route::post('/{newActivitie}', 'ActivityQuestionController@store')->name('books.question.store'); 
                Route::get('/{newActivitie}', 'ActivityQuestionController@create')->name('books.question.create');
                Route::get('/{activityQuestion}/edit', 'ActivityQuestionController@edit')->name('books.question.edit');
                Route::post('/{activityQuestion}/update', 'ActivityQuestionController@update')->name('books.question.update');
                Route::delete('/{activityQuestion}/delete', 'ActivityQuestionController@destroy')->name('books.question.destroy');                
                Route::prefix('{activityQuestion}/question-detail')->group(function () {
                    Route::get('/', 'QuestionDetailController@index')->name('books.detail.index');
                    Route::post('/store', 'QuestionDetailController@store')->name('books.detail.store');
                    Route::delete('/{questionDetail}/delete', 'QuestionDetailController@destroy')->name('books.detail.delete');
                    Route::post('/{questionDetail}/update', 'QuestionDetailController@update')->name('books.detail.update');
                });
            });
            Route::prefix('/exam')->group(function () {
                Route::get('/{newActivitie}', 'ExamController@show')->name('books.exam.show');
                Route::post('/{newActivitie}', 'ExamController@store')->name('books.exam.store');
            });
                    
        });
        Route::prefix('/score-activity')->group(function(){
            Route::get('/{newActivitie}/{student}/show', 'ScoreActivityController@show')->name('books.scoreActivity.show');

        });
        

        Route::get('/', 'BookController@index')->name('books.index');
        Route::get('/create', 'BookController@create')->name('books.create');
        Route::get('/{book}', 'BookController@show')->name('books.show');
        Route::post('/', 'BookController@store')->name('books.store');

        Route::prefix('/{book}/modules/')->group(function () {
            Route::get('/', 'ModuleController@index')->name('books.modules.index');
            Route::get('/create', 'ModuleController@create')->name('books.modules.create');
            Route::post('/', 'ModuleController@store')->name('books.modules.store');
            Route::post('/activity', 'ModuleController@activity')->name('books.modules.activity');
        });
        Route::prefix('/activity')->group(function () {
            Route::post('/{book}/{module}', 'ActivityController@store')->name('books.modules.activity.store');
            Route::post('/{book}/{module}/test', 'ActivityController@test')->name('books.modules.activity.test');
        });
        Route::prefix('/qualification')->group(function () {
            Route::get('/{book}/qualification/', 'QualificationController@index')->name('books.qualification.index');
        });
        
       
    });
});
