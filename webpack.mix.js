const tailwindcss = require('tailwindcss');
const mix = require('laravel-mix');
require('laravel-mix-purgecss');

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .options({
         processCssUrls: false,
         postCss: [ tailwindcss('tailwind.config.js') ],
   }).purgeCss();

mix.copyDirectory('resources/img', 'public/img');

if (mix.inProduction()) {
    mix.version();
}
