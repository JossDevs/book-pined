<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sushi\Sushi;

class InputType extends Model
{
    use Sushi;
    public $incrementing = false;

    protected $rows = [
        [
            'id' => 'text',
            'name' => 'Texto',
        ],
        [
            'id' => 'radio',
            'name' => 'Radio',
        ],
        [
            'id' => 'checkbox',
            'name' => 'Chequear',
        ],        
        [
            'id' => 'texarea',
            'name' => 'Parrafo',
        ],

    ];
}
