<?php

namespace App\Presenters;

use App\Media;

class MediaPresenter
{
    public $media;

    public function __construct(Media $media)
    {
        $this->media = $media;
    }

    public function getUrl()
    {
        return $this->media->getFullUrl();
    }
}
