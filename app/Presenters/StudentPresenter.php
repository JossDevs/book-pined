<?php

namespace App\Presenters;

use App\Student;

class StudentPresenter
{
    public $student;

    public function __construct(Student $student)
    {
        $this->student = $student;
    }

    public function name()
    {
        return "{$this->student->last_name} {$this->student->first_name}";
    }

}
