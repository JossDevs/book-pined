<?php

namespace App\Presenters;

use App\Book;

class BookPresenter
{
    public $book;

    public function __construct(Book $book)
    {
        $this->book = $book;
    }

    public function name()
    {
        return $this->book->name;
    }

    public function activities()
    {
        return $this->book->activities;
    }

    public function coverUrl()
    {
        if ($this->book->media->first() != null) {
            return $this->book->media->first()->getUrl();
        }

        return env('APP_URL')."/img/cover_callback_books.jpg";
    }


}
