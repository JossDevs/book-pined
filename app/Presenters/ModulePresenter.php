<?php

namespace App\Presenters;

use App\Module;

class ModulePresenter
{
    public $module;

    public function __construct(Module $module)
    {
        $this->module = $module;
    }

    public function name()
    {
        return $this->module->name;
    }
    public function id()
    {
        return $this->module->id;
    }

    public function images()
    {
        return optional($this->module->getMedia('images'));
    }
}
