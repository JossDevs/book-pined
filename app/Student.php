<?php

namespace App;

use App\Presenters\StudentPresenter;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'identification_number',
        'email',
        'password',
    ];

    public function present()
    {
        return new StudentPresenter($this);
    }

    public function books()
    {
        return $this->belongsToMany(Book::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function qualification()
    {
        return $this->hasMany(Qualification::class);
    }
}
