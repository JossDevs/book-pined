<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityQuestion extends Model
{
    //
    protected $fillable = [
        'name',
        'input_type',
        'score',
        'new_activitie_id'
    ];
    public function details()
    {
        return $this->hasMany(QuestionDetail::class);
    }
    public function newActivitie()
    {
        return $this->belongsTo(NewActivitie::class);
    }
    
    
}
