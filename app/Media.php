<?php

namespace App;

use App\Presenters\MediaPresenter;
use Spatie\MediaLibrary\MediaCollections\Models\Media as BaseMedia;

class Media extends BaseMedia
{
    public function present()
    {
        return new MediaPresenter($this);
    }
}
