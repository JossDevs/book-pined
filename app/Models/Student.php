<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Student
 *
 * @property $id
 * @property $first_name
 * @property $last_name
 * @property $identification_number
 * @property $email
 * @property $email_verified_at
 * @property $password
 * @property $user_id
 * @property $remember_token
 * @property $created_at
 * @property $updated_at
 *
 * @property BookStudent[] $bookStudents
 * @property Exam[] $exams
 * @property Qualification[] $qualifications
 * @property TestStudent[] $testStudents
 * @property User $user
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Student extends Model
{
    
    static $rules = [
		'first_name' => 'required',
		'last_name' => 'required',
		'identification_number' => 'required',
		'email' => 'required',
		'user_id' => 'required',
        'password' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name','last_name','identification_number','email','user_id','password'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookStudents()
    {
        return $this->hasMany('App\Models\BookStudent', 'student_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function exams()
    {
        return $this->hasMany('App\Models\Exam', 'student_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function qualifications()
    {
        return $this->hasMany('App\Models\Qualification', 'student_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function testStudents()
    {
        return $this->hasMany('App\Models\TestStudent', 'student_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
    

}
