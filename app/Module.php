<?php

namespace App;

use App\Presenters\ModulePresenter;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Module extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $fillable = [
        'name',
        'body',
        'type',
        'book_id'
    ];

    public function present()
    {
        return new ModulePresenter($this);
    }

    public function book()
    {
        return $this->belongsTo(Book::class);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('images')
            ->useDisk('books');
    }

    public function saveImages($requestName)
    {
        $this->addAllMediaFromRequest($requestName)
            ->each(function ($fileAdder) {
                $fileAdder->toMediaCollection('images');
            });
    }

    public function images()
    {
        return $this->getMedia('images');
    }
    public function activities()
    {
        return $this->hasMany(Activity::class);
    }
    public function qualification()
    {
        return $this->belongsTo(Qualification::class);
    }
    public function newActivities()
    {
        return $this->hasMany(NewActivitie::class);
    }
}
