<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionDetail extends Model
{
    protected $fillable = [
        'detail',
        'is_correct',
        'score',
        'activity_question_id'
    ];
    public function exam()
    {
        return $this->hasMany(Exam::class);
    }
    public function examStudent()
    {
        return $this->hasOne(Exam::class)   
        ->where('student_id' , auth()->user()->student->id);
    }
}
