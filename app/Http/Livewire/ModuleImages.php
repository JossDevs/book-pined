<?php

namespace App\Http\Livewire;

use App\Module;
use Livewire\Component;

class ModuleImages extends Component
{
    public $moduleId;
    public $index;
    public $module;

    public function mount($moduleId)
    {
        $module = Module::findOrFail($moduleId);
        $this->module =  $module;
        $this->index = 0;
    }

    public function render()
    {
        $module = $this->module;

        $buttonClassActive = 'relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150';

        $buttonClassInactive = 'relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-default leading-5 rounded-md';

        return view('livewire.module-images')
            ->with([
                'module' => $module,
                'buttonClassActive' => $buttonClassActive,
                'buttonClassInactive' => $buttonClassInactive,
            ]);
    }

    public function nextImage()
    {
        if (($this->module->images()->count() !== $this->index + 1)) {
            $this->index++;
        }
        
        return null;
    }

    public function previousImage()
    {
        if (($this->index !== 0)) {
            $this->index--;
        }
        
        return null;
    }
}
