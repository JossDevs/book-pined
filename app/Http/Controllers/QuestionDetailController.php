<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QuestionDetailController extends Controller
{
    public function index(){
        
        $activityQuestion = request()->activityQuestion; 
        $questionDetails = $activityQuestion->details;

        return view('books.modules.new-activities.questions.question-detail.index')
            ->with([
                'questionDetails' => $questionDetails
            ]);
    }
    public function store()
    {
        request()->validate([
            'detail' => [
                'required',
            ],
            'is_correct' => [
               'required'
            ],
        ]);

        $activityQuestion = request()->activityQuestion;
        return   $activityQuestion->details()->create([
            'detail'        => request()->detail,
            'is_correct'    => request()->is_correct,
            ]);

    }
    public function destroy(){
        $detail = request()->questionDetail;

        return $detail->delete();
       
    }
    public function update(){
        $detail = request()->questionDetail;
        return $detail->update([
            'detail'        => request()->detail,
            'is_correct'    => request()->is_correct,
            ]);
       
    }
}
