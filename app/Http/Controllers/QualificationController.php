<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;

class QualificationController extends Controller
{
    public function index(){
       
        $book = request()->book;
        $modules = $book->modules();
        if (auth()->user()->hasRole('student')) {
            $qualifications = auth()->user()->student->qualification->whereIn('module_id',$modules->pluck('id'));        
        }
        return view('qualifications.index')
        ->with([
            'book'              => $book,
            'modules'           => $modules,
            'qualifications'    => $qualifications
        ]);
    }
}
