<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Activity;
use App\Qualification;


class ActivityController extends Controller
{
    public function store()
    {   //dd(request());
        $book = request()->book;
        $module = request()->module;
        $activitys =$module->activities()->pluck('id');
        //dd($activity);
        for ($i=1; $i <= request()->numero; $i++) { 
            $activity_save = Activity::whereIn('id',$activitys)
            ->where('input_name','pregunta'.$i)->first();            
           if ($activity_save=='') {
                $activity_save = $module->activities()->create([
                    'input_name' => 'pregunta'.$i,
                    'answer' => request()['pregunta'.$i],
                    'percentage' => isset(request()['p_pregunta'.$i]) ? request()['p_pregunta'.$i] : null
                ]);
            }else{
                    $activity_save->input_name  = 'pregunta'.$i;
                    $activity_save->answer = request()['pregunta'.$i];
                    $activity_save->percentage = isset(request()['p_pregunta'.$i]) ? request()['p_pregunta'.$i] : null;
                    $activity_save->save();                
            }
            
        }

        return redirect()            
        ->route('books.modules.index', [$book, 'page='. request()->pagina])
        ->with('flash_success', 'Actividad creada con éxito.');
        
    }
    public function test()
    {  
        $user = auth()->user();
        $student =$user->student()->first();
        //dd($student->id);
        $book = request()->book;
        $module = request()->module;
        $activities =$module->activities()->get();
        $acum_nota = 0;
        foreach ($activities as $activity) {   
            

          $test = $activity->test()->where('student_id',$student->id)->get();
          if (count($test) == 0) {
              //dd($test);
              $test = $activity->test()->create([     
                'input_name'    => $activity->input_name,       
                'answer'        => request()[$activity->input_name],
                'percentage'    => $activity->percentage,
                'score'         => request()[$activity->input_name] == $activity->answer ? $activity->percentage/10 : 0,
                'student_id'    => $student->id,
                ]);
                $acum_nota += request()[$activity->input_name] == $activity->answer ? $activity->percentage/10 : 0;
          }else{
              $test = $test->first();
              $test->input_name =   $activity->input_name;
              $test->answer     =   request()[$activity->input_name];
              $test->percentage =   $activity->percentage;
              $test->score      =   request()[$activity->input_name] == $activity->answer ? $activity->percentage/10 : 0;
              $test->save();
              $acum_nota += request()[$activity->input_name] == $activity->answer ? $activity->percentage/10 : 0;
          }                     
        }
        $qualifications = auth()->user()->student->qualification->where('module_id',$module->id);        
        $qualifications = Qualification::updateOrCreate(
        ['module_id' => $module->id,
        'student_id' => $student->id],
        ['nota' => $acum_nota , 
        'allow_test' => 1]
        );      
         return redirect()            
        ->route('books.modules.index', [$book, 'page='. request()->pagina])
        ->with('flash_success', 'Actividad guardada con éxito.');
        
    }
}
