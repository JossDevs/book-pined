<?php

namespace App\Http\Controllers;

use App\ActivityQuestion;
use Illuminate\Http\Request;

class ActivityQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newActivitie = request()->newActivitie;
        return view('books.modules.new-activities.questions.index')
            ->with([
                'activitie' => $newActivitie,
            ]);
    }
    public function store()
    {   
        $n_inputs = request()->n_inputs;
        $newActivitie = request()->newActivitie;
        //dd($n_inputs);
        request()->validate([
            'name' => [
                'required',
            ],
            'input_type' => [
                'required',
            ],
        ]);
        //dd(request()->all());
       $question = $newActivitie->questions()->create([
        'name'              =>request()->name,
        'input_type'         =>request()->input_type,
       
            ]);
        for ($i=0; $i < $n_inputs; $i++) { 
            foreach (request()->check as $correcta) {
                if ($correcta == $i) {
                 $val = true;
                }
                
            }
            $question->details()->updateOrCreate([
                'detail'        => request()->$i,
                'score'    => request()->score_[$i] ?? null,
                'is_correct'    => $val ?? false
                ]); 
                $val = false;           
        }
        
        return  redirect()
        ->route('books.newActivitie.show', $newActivitie)
        ->with('flash_success', 'Pregunta creada con éxito.'); 
    }
    public function destroy()
    {
        $question = request()->activityQuestion;
        $newActivitie = $question->newActivitie;
        $question->delete();

        return  redirect()
        ->route('books.newActivitie.show', $newActivitie)
        ->with('flash_success', 'Pregunta eliminada con éxito.'); 
    }
    public function create()
    {
        $newActivitie = request()->newActivitie;
        $question = new ActivityQuestion();
        return view('books.modules.new-activities.questions.create')
            ->with([
                'activitie' => $newActivitie,
                'question' => $question
            ]);
    }
    public function edit()
    {
        $question = request()->activityQuestion;
        $newActivitie = $question->newActivitie;
        return view('books.modules.new-activities.questions.edit')
            ->with([
                'activitie' => $newActivitie,
                'question' => $question
            ]);
    }
    public function update()
    {
        $question = request()->activityQuestion;
        $newActivitie = $question->newActivitie;
        request()->validate([
            'name' => [
                'required',
            ],
            'input_type' => [
                'required',
            ],
        ]);
        $question->update([
        'name'              =>request()->name,
        'input_type'         =>request()->input_type,
            ]);        
        return  redirect()
        ->route('books.newActivitie.show', $newActivitie)
        ->with('flash_success', 'Pregunta actualizada con éxito.'); 
    }

    
}
