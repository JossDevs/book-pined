<?php

namespace App\Http\Controllers;

use App\Book;

class BookController extends Controller
{
    public function index()
    {
        $books = Book::all();

        if (auth()->user()->hasRole('student')) {
            $books = auth()->user()->student->books;
        }

        return view('books.index')
            ->with([
                'books' => $books
            ]);
    }

    public function show()
    {
        $book = request()->book;

        return view('books.show')
            ->with([
                'book' => $book,
            ]);
    }

    public function create()
    {
        return view('books.create');
    }

    public function store()
    {
        request()->validate([
            'name' => [
                'required',
            ],
            'cover' => [
                'required',
            ],
        ]);

        $book = new Book;
        $book->name = request()->name;
        $book->saveCover(request()->cover);

        $book->save();

        return redirect()
            ->route('books.index')
            ->with('flash_success', 'Libro creado con éxito.');
    }
}
