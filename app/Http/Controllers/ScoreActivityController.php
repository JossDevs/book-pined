<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ScoreActivityController extends Controller
{
    public function show()
    {
        $newActivitie = request()->newActivitie; 
        $student = request()->student;       
        $module = $newActivitie->module;       
        $book = $module->book;        
        $questions = $newActivitie->questions;
        return view('books.modules.score-activitys.show')
        ->with([
            'student' => $student,
            'newActivitie' =>$newActivitie,
            'questions' =>$questions,
            'module' => $module,
            'book' => $book
        ]);

    }
}
