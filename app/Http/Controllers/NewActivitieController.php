<?php

namespace App\Http\Controllers;

use App\NewActivitie;
use Illuminate\Http\Request;

class NewActivitieController extends Controller
{
    public function index()
    {       
        $module = request()->module;
        $newActivities = $module->newActivities;
        return view('books.modules.new-activities.index')
            ->with([
                'newActivities' => $newActivities
            ]);
       
    }
    public function create()
    {
        
        $module = request()->module;
        $book = $module->book;
        return view('books.modules.new-activities.create')
        ->with([
            'module' => $module,
            'book' => $book
        ]);
    }
    public function store()
    {
        $module = request()->module;
        $book = $module->book;
        request()->validate([
            'name' => [
                'required',
            ],
        ]);
    
       $newActivitie= $module->newActivities()->create([
            'name'          => request()->name,
            'user_id'       => auth()->user()->id,
            'module_id'    =>  $module->id
            ]);
            
        return  redirect()
        ->route('books.newActivitie.show', $newActivitie);
    }
    public function show()
    {
        $newActivitie = request()->newActivitie;
        $module = $newActivitie->module;
        $book = $module->book;
        return view('books.modules.new-activities.show')
        ->with([
            'newActivitie' => $newActivitie,
            'module' => $module,
            'book' => $book
        ]);
    }
    public function destroy()
    {
        $newActivitie = request()->newActivitie;
        $module = $newActivitie->module;
        $book = $module->book;        
        $newActivitie->delete();
        return redirect()
        ->route('books.newActivitie.create', [ $module, $book])
        ->with('flash_success', 'Actividad eliminada con éxito.'); 
    }
}
