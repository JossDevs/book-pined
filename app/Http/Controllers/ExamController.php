<?php

namespace App\Http\Controllers;

use App\Exam;
use Illuminate\Http\Request;

class ExamController extends Controller
{
    public function show()
    {        
        $newActivitie = request()->newActivitie;        
        $module = $newActivitie->module;       
        $book = $module->book;        
        $questions = $newActivitie->questions;
        return view('books.modules.exam.show')
        ->with([
            'newActivitie' =>$newActivitie,
            'questions' =>$questions,
            'module' => $module,
            'book' => $book
        ]);
    }
    public function store()
    {  
        //dd(request()->all());
        $newActivitie = request()->newActivitie;
        $questions = $newActivitie->questions;
        $student = auth()->user()->student;
        $questions->each(function($questionId) use($student){ 
            // todas las respuestas anteriores pasan a is_active = false    
            $detail = $questionId->details;        
            $detail->each(function($detailId){                
                $examD = $detailId->examStudent;           
                if($examD != null){                            
                $examD->update([
                    'is_active' => false
                ]);
                }
            });
            $nombre = $questionId->id;
            if ( is_array(request()->$nombre)  ) {
                $array_respuestas = collect(request()->$nombre);
                $array_respuestas->each(function($array_respuestasId) use ($detail, $student){
                    $exam_detalle = $detail->where('detail',$array_respuestasId)->first();
                    if($exam_detalle != null){
                        Exam::updateOrCreate(['question_detail_id'=>$exam_detalle->id,
                        'student_id' => $student->id],
                        [
                            'detail' => $exam_detalle->detail,
                            'score' => $exam_detalle->score,
                            'is_active' => true
                        ]); 
                    }
                });
            }elseif(isset(request()->$nombre)){         
               if(count($detail)>1){
               $detail->each(function($detailId) use($student, $nombre){    
                Exam::updateOrCreate(['question_detail_id'=>$detailId->id,
                'student_id' => $student->id],
                [
                    'detail' =>$detailId->detail , 
                    'score' =>$detailId->score,
                    'is_active' => $detailId->detail == request()->$nombre ?  true:false
                ]);   
            });
            }else{
                Exam::updateOrCreate(['question_detail_id'=>$detail->first()->id,
                'student_id' => $student->id],
                [
                    'detail' => request()->$nombre ?? '',
                    'score' => $detail->first()->score,
                    'is_active' => true
                ]); 
                } 
            }
        });
        return redirect()->route('books.scoreActivity.show', [$newActivitie,$student])
        ->with('flash_success', 'Prueba terminada.');
    }
}
