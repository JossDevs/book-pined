<?php

namespace App\Http\Controllers;

use App\Book;
use App\Student;

class BookAssignmentController extends Controller
{
    public function index()
    {
        $books = Book::all();

        return view('books.assignment.index')
            ->with([
                'books' => $books
            ]);
    }

    public function show()
    {
        $book = request()->book;

        $students = Student::all();

        return view('books.assignment.show')
            ->with([
                'book' => $book,
                'students' => $students,
            ]);
    }

    public function store()
    {
        $book = request()->book;

        $student = request()->student;

        $student->books()->attach([$book->id]);

        return redirect()
            ->route('books.assignment.show', $book)
            ->with('flash_success', 'Libro asignado con éxito.');
    }

    public function destroy()
    {
        $book = request()->book;

        $student = request()->student;

        $student->books()->detach([$book->id]);

        return redirect()
            ->route('books.assignment.show', $book)
            ->with('flash_success', 'Se removio el libro con éxito.');
    }
}
