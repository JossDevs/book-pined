<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use App\Activity;
use App\TestStudent;

class ModuleController extends Controller
{
    public function index()
    {
        $user = auth()->user()->hasRole('student');
        $book = request()->book;
        $modules = $book->modules()
            ->simplePaginate(1);
            $module = $modules->first(); 
        $activities = $modules->first()->activities()->get();
        if ($user) {
        $student =auth()->user()->student()->first();
        $test = TestStudent::where('student_id',$student->id)
        ->wherein('activity_id', $activities->pluck('id') )->get();
            if (count($test)>0) {
                # code...
                //dd(count($test));
                $test_exists = $test;
            }
        
        }
            
            
        return view('books.modules.index')
            ->with([
                'book'          => $book,
                'modules'       => $modules,                
                'test'          => isset($test_exists)? $test: null,
                'activities'    => $activities
            ]);
    }

    public function create()
    {

        $book = request()->book;
        return view('books.modules.create')
            ->with([
                'book' => $book,
            ]);
    }

    public function store()
    {
        request()->validate([
            'name' => [
                'required',
                function ($attr, $value, $fail) {
                    if (request()->body === null && request()->file === null) {
                        $fail('Por favor, escoge que tipo de contenido que deseas subir.');
                    }
                },
            ],
            'body' => [
                Rule::requiredIf(request()->file == null)
            ],
            'file' => [
                Rule::requiredIf(request()->body == null)
            ],
        ]);

        $book = request()->book;

        $module = $book->modules()->create([
            'name' => request()->name,
            'body' => request()->body,
            'type' => request()->type
        ]);

        $module->saveImages('file');

        return redirect()
            ->route('books.show', $book)
            ->with('flash_success', 'Módulo creado con éxito.');
    }
    public function activity()
    {
        $book = request()->book;
        $activity = 70;
        $pagina = request('page');

        dd(pagina);
        return redirect()            
        ->route('books.modules.index', [$book, 'page=1', $activity])
        ->with('flash_success', 'Actividad creada con éxito.');
    }
}
