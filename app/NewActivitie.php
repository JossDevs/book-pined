<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewActivitie extends Model
{
    protected $fillable = [
        'name',
        'user_id',
        'module_id'
    ];
    public function questions()
    {
        return $this->hasMany(ActivityQuestion::class);
    }
    public function module()
    {
        return $this->belongsTo(Module::class);
    }

}
