<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{   //public $table = "qualifications";
    protected $fillable = [
        'nota',
        'allow_test',
        'module_id',
        'student_id',
    ];
    public function module()
    {
        return $this->belongsTo(Module::class);
    }
}
