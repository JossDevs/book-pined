<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    //
    protected $fillable = [
        'input_name',
        'answer',
        'percentage',
        'module_id'
    ];
    public function module_activity()
    {
        return $this->belongsTo(Module::class);
    }
    public function test()
    {
        return $this->hasMany(TestStudent::class);
    }
}
