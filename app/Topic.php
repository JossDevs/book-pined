<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $fillable = [
        'name',
        'anchor_text',
        'module_id',
    ];

    public function setAnchorTextAttribute($value)
    {
        $valueLowerCase = strtolower($value);

        $value = str_replace(' ', '_', $valueLowerCase);

        $this->attributes['anchor_text'] = ($value);
    }
}
