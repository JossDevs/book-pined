<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestStudent extends Model
{
    protected $fillable = [
        'answer',
        'input_name',
        'percentage',
        'score',
        'activity_id',
        'student_id'
    ];
    public function activity_test()
    {
        return $this->belongsTo(Module::class);
    }
}
