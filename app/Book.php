<?php

namespace App;

use App\Presenters\BookPresenter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Book extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $fillable = [
        'name',
    ];

    public function present()
    {
        return new BookPresenter($this);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('cover')
            ->useDisk('books');
    }

    public function saveCover(UploadedFile $image)
    {
        return $this->addMedia($image)
            ->toMediaCollection('cover');
    }

    public function modules()
    {
        return $this->hasMany(Module::class);
    }

    public function students()
    {
        return $this->belongsToMany(Student::class);
    }
}
