<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $fillable = [
        'detail',
        'score',
        'question_detail_id',
        'student_id',
        'is_active'
    ];

}
