@extends('layouts.books-show')
@section('books-content')
@php
    $breadcrumbs = collect([
        [
            'title' => 'Mis libros',
            'url' => '#',
        ]
    ]);
@endphp
<x-ui.flash />
<div class="space-y-3">
    <div class="text-right">
        @role('admin')
            <a 
                href="{{route('books.assignment.index')}}"
                type="button"
                class="inline-flex items-center px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:shadow-outline-indigo focus:border-indigo-700 active:bg-indigo-700 transition duration-150 ease-in-out">
                Asignamiento de libros
            </a>
            <a 
                href="{{route('books.create')}}"
                type="button"
                class="inline-flex items-center px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:shadow-outline-indigo focus:border-indigo-700 active:bg-indigo-700 transition duration-150 ease-in-out">
                Agregar Libro
            </a>
            <a 
                href="{{route('students.create')}}"
                type="button"
                class="inline-flex items-center px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:shadow-outline-indigo focus:border-indigo-700 active:bg-indigo-700 transition duration-150 ease-in-out">
                Agregar Estudiante
            </a>
        @endrole
    </div>
    <div class="grid gap-8 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-6">
        @foreach ($books as $book)
            <a class="flex flex-col items-center" href="{{route('books.show', $book)}}">
                <p class="mb-2">{{$book->present()->name()}}</p>
                <img src="{{$book->present()->coverUrl()}}" alt="{{$book->present()->coverUrl()}}">
            </a>
        @endforeach

    </div>
    <x-ui.empty-records :count="count($books)">
        No existen libros registrados aún.
    </x-ui.empty-records>
</div>
@endsection
