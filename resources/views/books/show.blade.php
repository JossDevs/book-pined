@extends('layouts.books-show')
@section('content')
@php
    $breadcrumbs = collect([
        [
            'title' => 'Mis libros',
            'url' => route('books.index'),
        ],
        [
            'title' => $book->present()->name(),
            'url' => '#',
        ]
    ]);
@endphp
<x-ui.flash/>
<div class="text-right">
    @role('admin')
        <a href="{{route('books.modules.create', $book)}}" type="button" class="inline-flex items-center px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:shadow-outline-indigo focus:border-indigo-700 active:bg-indigo-700 transition duration-150 ease-in-out">
            Agregar Módulo
        </a>
    @endrole
</div>

<div class="h-8"></div>

<div class="grid grid-cols-4 gap-6">
    <div class="hidden sm:block">
        <figure>
            <img class="w-full" src="{{$book->present()->coverUrl()}}" alt="{{$book->present()->coverUrl()}}">
        </figure>
    </div>
    <div class="bg-white shadow overflow-hidden col-span-4 rounded-lg sm:col-span-3" style="align-self: start;">
        <div class="px-4 py-5 border-b border-gray-200 sm:px-6">
            <h3 class="text-lg leading-6 font-medium text-gray-900">
                Libro {{$book->present()->name()}}
            </h3>
            <p class="mt-1 max-w-2xl text-sm leading-5 text-gray-500">
                Hemafre Ediciones.
            </p>
        </div>
        <div>
            <dl>
                <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm leading-5 font-medium text-gray-500">
                        Modulos
                    </dt>
                    <dd class="mt-1 text-sm leading-5 text-gray-900 sm:mt-0 sm:col-span-2">
                        <ul class="{{$book->modules->count() > 0 ? 'border border-gray-200' : ''}} rounded-md">
                            @foreach ($book->modules as $module)
                                <li class=" {{$loop->index > 0 ? 'border-gray-200 border-t' : ''}} pl-3 pr-4 py-3 flex items-center justify-between text-sm leading-5">
                                    <div class="w-0 flex-1 flex items-center">
                                        <a href="{{route('books.modules.index' , [$book, 'page='.$loop->iteration])}}" class="ml-2 flex-1 w-0 truncate">
                                            {{$module->present()->name()}}
                                            
                                            
                                        </a>
                                    </div>
                                    <div class="ml-4 flex-shrink-0">
                                        <a href="{{route('books.modules.index', [$book, 'page='.$loop->iteration, $module->id])}}"
                                            class="font-medium text-indigo-600 hover:text-indigo-500 transition duration-150 ease-in-out">
                                            <i class="ml-2 fas fa-arrow-right"></i>
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                            @role('student')
                            <li class="border-gray-200 border-t pl-3 pr-4 py-3 flex items-center justify-between text-sm leading-5">
                                <div class="w-0 flex-1 flex items-center">
                                    <a href="{{route('books.qualification.index', [$book])}}" class="ml-2 flex-1 w-0 truncate">
                                        Calificaciones   
                                    </a>
                                </div>
                                <div class="ml-4 flex-shrink-0">
                                    <a href="{{route('books.qualification.index', [$book])}}"
                                        class="font-medium text-indigo-600 hover:text-indigo-500 transition duration-150 ease-in-out">
                                        <i class="ml-2 fas fa-chart-bar"></i>
                                    </a>
                                </div>
                            </li>
                            @endrole
                        </ul>
                        <x-ui.empty-records :count="$book->modules->count()">
                            No se han registrado modulos.
                        </x-ui.empty-records>
                    </dd>
                </div>
            </dl>
        </div>
    </div>
</div>
@endsection
