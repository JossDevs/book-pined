@extends('layouts.books-show')
@section('books-content')
@php
    $breadcrumbs = collect([
        [
            'title' => 'Mis libros',
            'url' => route('books.index'),
        ],
        [
            'title' => 'Asignamiento de libros',
            'url' => '#',
        ],
    ]);
@endphp
<x-ui.flash />
<div class="space-y-3">
    <div class="flex flex-col">
        <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
            <div class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                <table class="min-w-full">
                    <thead>
                        <tr>
                            <th
                                class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Nombre
                            </th>
                            <th
                                class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Total Asignamientos
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50"></th>
                        </tr>
                    </thead>
                    <tbody class="bg-white">
                        @foreach ($books as $book)
                            <tr>
                                <td
                                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900 flex items-center space-x-3">
                                    <img class="w-5" src="{{$book->present()->coverUrl()}}" alt="{{$book->present()->coverUrl()}}">
                                    <p>{{$book->present()->name()}}</p>
                                </td>
                                <td
                                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                    {{$book->students->count()}}
                                </td>
                                <td
                                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                    <div class="flex justify-end items-center space-x-8">
                                        <a href="{{route('books.assignment.show', $book)}}"
                                           class="text-teal-600 hover:text-teal-900 focus:outline-none focus:underline">
                                            Asignar
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <x-ui.empty-records :count="count($books)">
        No existen libros registrados aún.
    </x-ui.empty-records>
</div>
@endsection