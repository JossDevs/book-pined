@extends('layouts.books-show')
@section('books-content')
@php
    $breadcrumbs = collect([
        [
            'title' => 'Mis libros',
            'url' => route('books.index'),
        ],
        [
            'title' => 'Asignamiento de libros',
            'url' => route('books.assignment.index'),
        ],
        [
            'title' => 'Alumnos',
            'url' => '#',
        ],
    ]);
@endphp
<x-ui.flash />
<div class="space-y-5">
    <div class="flex flex-col">
        <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
            <div class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                <table class="min-w-full">
                    <thead>
                        <tr>
                            <th
                                class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Nombre
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50"></th>
                        </tr>
                    </thead>
                    <tbody class="bg-white">
                        <tr>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900 flex items-center space-x-3">
                                <img class="w-5" src="{{$book->present()->coverUrl()}}" alt="{{$book->present()->coverUrl()}}">
                                <p>{{$book->present()->name()}}</p>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="space-y-2">
        <h3 class="text-lg leading-6 font-medium text-gray-900">
            Alumnos
        </h3>
        <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
            <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded">
                <table class="min-w-full">
                    <tbody class="bg-white">
                        @foreach ($students as $student)
                            <tr height="5"></tr>
                            <tr>
                                <td
                                    class="shadow px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                                    <div class="flex justify-between">
                                        <div class="space-x-2 flex items-center">
                                            <img class="h-8 w-8 rounded-full" src="https://ui-avatars.com/api/?name=Prof. Destini Kessler III"
                                                alt="Prof. Destini Kessler III">
                                            <div>
                                                {{$student->present()->name()}}
                                            </div>
                                        </div>
                                        <div>
                                            @if ($student->books->contains($book->id))
                                                <form action="{{route('books.assignment.destroy', [$book, $student])}}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit"
                                                        class="py-2 px-4 border border-gray-300 rounded-md text-sm leading-5 font-medium text-gray-700 bg-white hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out">
                                                        <i class="fas fa-times mr-2 text-red-500"></i>Quitar
                                                    </button>
                                                </form>
                                            @else
                                                <form action="{{route('books.assignment.store', [$book, $student])}}" method="post">
                                                    @csrf
                                                    <button 
                                                        type="submit" 
                                                        class="py-2 px-4 border border-gray-300 rounded-md text-sm leading-5 font-medium text-gray-700 bg-white hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out">
                                                        <i class="fas fa-plus mr-2 text-green-500"></i>Asignar
                                                    </button>
                                                </form>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
    
    
            </div>
        </div>
    </div>
</div>
@endsection