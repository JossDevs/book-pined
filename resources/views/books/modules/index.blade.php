@extends('layouts.books-show')
@section('content')
@php
    $breadcrumbs = collect([
        [
            'title' => 'Mis libros',
            'url' => route('books.index'),
        ],
        [
            'title' => $book->present()->name(),
            'url' => route('books.show', $book),
        ],
        [
            'title' =>'Modulos',
            'url' => '#',
        ]
    ]);
@endphp
<x-ui.flash />
<div class="flex">
    @include('components.menu-left')

    <div class="hidden lg:block lg:w-10"></div>
    <div class="bg-white shadow w-full sm:rounded-lg relative">
        <div class="lg:hidden mb-8 sticky py-6 bg-white px-8" style="top:-9px;">
            <select aria-label="Selected tab"
                onchange="window.location = this.value"
                class="form-select block w-full pl-3 pr-10 py-2 text-base leading-6 border-gray-300 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 sm:text-sm sm:leading-5 transition ease-in-out duration-150">
                @foreach ($book->modules as $module)
                    <option 
                        {{request('page') == $loop->index+1 ? 'selected' : ''}}
                        value="{{route('books.modules.index', [$book, "page=".($loop->index+1)])}}">
                        {{$module->present()->name()}}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="px-8 py-6">
            @foreach ($modules as $module)
                @if ($module->type === 'html')    
                <form  id="respuestas" style="display:none;"> 
                    @foreach ($activities as $inpt_act)
                    <input type="text" name="nom_{{$inpt_act->input_name}}" id="nom_{{$inpt_act->input_name}}"  value="{{$inpt_act->answer}}">
                    <input type="text" name="por_{{$inpt_act->input_name}}" id="por_{{$inpt_act->input_name}}"  value="{{$inpt_act->percentage}}">
                      
                    @endforeach
                </form>              
                    @role('admin')
                        <form action="{{route('books.modules.activity.store', [$book,$module])}}" id="{{$module->id}}" method="POST">
                    @endrole
                    @role('student')
                        <form action="{{route('books.modules.activity.test', [$book,$module])}}" id="{{$module->id}}" method="POST">
                    @endrole
                    <input type="hidden" name="numero" id="numero">
                    <input type="hidden" name="pagina" value="{{request('page')}}"> 
                    @csrf   
                    {!!$module->body!!}                
                    <br>
                    <div class="text-right">
                        @if ($test == null)
                        <button                        
                        type="submit"
                        class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-white bg-teal-600 hover:bg-teal-500 focus:outline-none focus:border-teal-700 focus:shadow-outline-teal active:bg-teal-700 transition ease-in-out duration-150">
                        Guardar
                    </button>
                            
                        @endif
                    </div>
                </form>               
                @endif

                @if ($module->type === 'images')
                    <livewire:module-images
                        :moduleId="$module->id"
                    >
                @endif
            @endforeach
            <div class="h-5"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        @role('admin')     
        var numero = -4;  
        var nombre = '';      
            $('#{{$module->id}} :input').each(function(index){ 
            $(this).addClass("addPorcentaje");            
            var botonPorcentaje = '<input type="number" class=" mb-2 clase-respuesta porcentaje"  placeholder="%" style="display:block; width : 60px;" name="p_'+$(this).attr('name')+'" id="p_'+$(this).attr('name')+'" >';
            
             var nombre_input = $(this).attr('name'); 
            if (nombre != nombre_input) {
                numero++;
            }  
             nombre = nombre_input;              
            if (($("input[name=nom_"+ nombre+"]").length > 0) && $(this).attr('type') !='radio' ) {
                $(this).val($('#nom_'+ nombre).val());
                    if (!$("#p_"+$(this).attr('name')).length) {       
                    $('#{{$module->id}}').find($(this)).after(botonPorcentaje);
                    $("#p_"+ $(this).attr('name')).val($("input[name=por_"+ nombre+"]").val());
                     }
                 
             } 
             if ($(this).attr('type') =='radio' || $(this).attr('type') =='checkbox') {
              
                if ($(this).val() == $("input[name=nom_"+ nombre+"]").val() ) {
                    var nom = 'nom_'+nombre;
                    $("input[name="+nombre+"][value="+ $("#"+ nom ).val()+"]").prop("checked",true); 
                    $('#{{$module->id}}').find($(this)).after(botonPorcentaje);
                    $("#p_"+ $(this).attr('name')).val($("input[name=por_"+ nombre+"]").val());  
                }
                  
             }             
            });
            $(".addPorcentaje").dblclick(function(e){
               var botonPorcentaje = '<input type="number" class=" clase-respuesta porcentaje"  placeholder="%" style="display:block; width : 60px;" name="p_'+$(this).attr('name')+'" id="p_'+$(this).attr('name')+'">';
                  
                        if (!$("#p_"+$(this).attr('name')).length) {       
                        $('#{{$module->id}}').find($(this)).after(botonPorcentaje);
                        $("#p_"+ $(this).attr('name')).focus();

                        }else{
                            $("#p_"+ $(this).attr('name')).focus();
                        }                     
                    
            });
            $('#numero').val(numero); 
            @endrole()  
            var test = @json($test);
        @role('student')     
        var numero = -4;  
        var nombre = '';   
            $('#{{$module->id}} :input').each(function(index){ 
                $(this).addClass("addPorcentaje");
                $(this).attr('id', $(this).attr('name'));
                
                var etiqueta = '<div class="clase-respuesta mb-2" id="res_'+$(this).attr('name')+'"><label id="etiqueta_'+$(this).attr('name')+'">Respuesta</label></div>';
                var porcentaje = '<div class="clase-respuesta mb-2" ><label id="p_'+$(this).attr('name')+'">Porcentaje: </label></div>';
                var nombre_input = $(this).attr('name'); 
                if (nombre != nombre_input) {
                    numero++;
                }  
                nombre = nombre_input;              
                    if (($("input[name=nom_"+ nombre+"]").length > 0) && $(this).attr('type') !='radio' ) {
                    
                            if (!$("#p_"+$(this).attr('name')).length) {    
                                if (test) {   // si viene un vlor de actividad por el estudiante
                            $('#{{$module->id}}').find($(this)).after(etiqueta).after(porcentaje);
                            $("#etiqueta_"+ nombre).text("Respuesta: "+$('#nom_'+ nombre).val());
                            $("#p_"+ nombre).text("Valor: "+$("input[name=por_"+ nombre+"]").val()+" %");
                                }
                            
                                $.each(test, function(i, item) {
                                if (test[i]['input_name'] == nombre) {                            
                                $("#"+nombre).val(test[i]['answer']);
                                if ($("#nom_"+nombre).val() == test[i]['answer'] ) {
                                    console.log($("input[name=por_"+ nombre+"]").val());
                                    $("#res_"+nombre).addClass("clase-respuesta-correcta");
                                }else {
                                    $("#res_"+nombre).addClass("clase-respuesta-incorrecta");
                                }
                                
                                }
                                });         
                                        
                            }
                        
                    } 
                    if ($(this).attr('type') =='radio' || $(this).attr('type') =='checkbox') {
                        var radio ='';
                                $.each(test, function(i, item) {
                                    if (test[i]['input_name'] == nombre) {
                                        radio = test[i]['answer'];
                                    }
                                });  
                            if ($(this).val() == radio ) {
                            var nom = 'nom_'+nombre;
                            $("input[name="+nombre+"][value="+radio+"]").prop("checked",true); 
                            $('#{{$module->id}}').find($(this)).after(etiqueta).after(porcentaje);
                            $("#p_"+ nombre).text("Porcentaje: "+$("input[name=por_"+ nombre+"]").val());                   
                            $("#etiqueta_"+ nombre).text("Respuesta: "+$("input[name=nom_"+ nombre+"]").val());
                            if ($("#nom_"+nombre).val() == radio ) {
                                    $("#res_"+nombre).addClass("clase-respuesta-correcta");
                                }else {
                                    $("#res_"+nombre).addClass("clase-respuesta-incorrecta");
                                }   
                        }
                        
                    }             
            });
            $('#numero').val(numero); 
            @endrole()    
    }); 
    </script>
@endsection




