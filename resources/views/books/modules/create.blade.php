@extends('layouts.books-show')
@section('content')
@php
    $breadcrumbs = collect([
        [
            'title' => 'Mis libros',
            'url' => route('books.index'),
        ],
        [
            'title' => $book->present()->name(),
            'url' => route('books.show', $book),
        ],
        [
            'title' =>'Crear Modulo',
            'url' => '#',
        ]
    ]);
@endphp
<form action="{{route('books.modules.store', $book)}}" method="POST" class="space-y-8" enctype="multipart/form-data">
    @csrf
    <div class="mt-6 grid row-gap-6 col-gap-4 grid-cols-1 sm:grid-cols-2" x-data="{typeContent : null}">
        <div>
            <label for="name" class="block text-sm font-medium leading-5 text-gray-700">
                Nombre del módulo.
            </label>
            <div class="mt-1 rounded-md shadow-sm">
                <input 
                    id="name" 
                    placeholder="Nombre del módulo" 
                    name="name" 
                    value="{{old('name')}}"
                    type="text"
                    class="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5">
            </div>
            <x-ui.error :errors="$errors" type="name" />
        </div>

        <div >
            <label for="document" class="block text-sm font-medium leading-5 text-gray-700">
                Escoje el tipo de contenido que deseas subir.
            </label>
            <div 
                x-model="typeContent" 
                name="type"
                class="mt-1 relative rounded-md shadow-sm">
                <select name="type" class="form-select text-sm block w-full">
                    <option value="">Escoje un tipo</option>
                    <option value="html">HTML</option>
                    <option value="images">Imagenes</option>
                </select>
            </div>
        </div>

        <div x-show="typeContent === 'html'" class="sm:col-span-6">
            <label for="body" class="block text-sm font-medium leading-5 text-gray-700">
                Contenido html
            </label>
            <div class="mt-1 rounded-md shadow-sm">
                <textarea 
                    id="body" 
                    rows="5" 
                    name="body" 
                    placeholder="Contenido del libro." 
                    class="form-textarea block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5">{{old('body')}}</textarea>
            </div>
            <x-ui.error :errors="$errors" type="body" />
        </div>

        <div x-show="typeContent === 'images'">
            <label for="file" class="block text-sm font-medium leading-5 text-gray-700">
                Imagenes
            </label>
            <div class="mt-1 relative rounded-md shadow-sm">
                <input 
                    id="file" 
                    name="file[]" 
                    type="file" 
                    accept="image/jpeg" 
                    class="form-input block w-full sm:text-sm sm:leading-5" 
                    style="padding: 0.35rem 0.5rem;"
                    multiple>
            </div>
            <x-ui.error :errors="$errors" type="file" />
        </div>

    </div>
    <div class="border-t border-gray-200 pt-5">
        <div class="flex justify-end">
            <div class="inline-flex rounded-md shadow-sm">
                <a href="{{route('books.show', $book)}}"
                    class="py-2 px-4 border border-gray-300 rounded-md text-sm leading-5 font-medium text-gray-700 bg-white hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out">
                    Cancelar
                </a>
            </div>
            <div class="ml-3 inline-flex rounded-md shadow-sm">
                <button type="submit"
                    class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-white bg-teal-600 hover:bg-teal-500 focus:outline-none focus:border-teal-700 focus:shadow-outline-teal active:bg-teal-700 transition ease-in-out duration-150">
                    Guardar
                </button>
            </div>
        </div>
    </div>
</form>
@endsection
