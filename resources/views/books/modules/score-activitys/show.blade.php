@extends('layouts.books-show')
@section('content')
@php
    $breadcrumbs = collect([
        [
            'title' => 'Mis libros',
            'url' => route('books.index'),
        ],
        [
            'title' => $book->present()->name(),
            'url' => route('books.show', $book),
        ],
        [
            'title' => $module->present()->name(),
            'url' => route('books.modules.index', [$book]),
        ]
    ]);
@endphp
<x-ui.flash />
<div class="flex">
    @include('components.menu-left')   

    <div class="hidden lg:block lg:w-10"></div>
    <div class="bg-white shadow w-full sm:rounded-lg relative">
        <div class="px-4 py-5 border-b border-gray-200 sm:px-6">
            <h3 class="text-lg leading-6 font-medium text-gray-900">
                Actividad: {{$newActivitie->name}}
            </h3>
        </div>
        @foreach ($questions as $key=>$question)
        <div class="shadow px-6 py-4 bg-blue-50 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
            <div class="flex justify-between">
                <div class="space-x-2 flex items-center">
                    <img class="h-8 w-8 rounded-full" src="https://ui-avatars.com/api/?name={{$loop->iteration}}"
                        alt="{{$loop->iteration}}">
                    <div>
                            {{$question->name}}
                    </div>
                </div>
                <div>
                </div>
            </div>
        </div>
        @php
            $suma = 0;
        @endphp
        <div class="shadow px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                <div class=" justify-between">                    
                    @foreach ($question->details as $detail)
                            @php
                                if($detail->is_correct && $detail->examStudent->is_active && $detail->examStudent->detail == $detail->detail ){
                                    $suma += $detail->examStudent->score;
                                } 
                            @endphp
                    <div class="relative flex p-2 border rounded-tl-md rounded-tr-md mt-1 {{$detail->is_correct ? 'bg-green-100' : ($detail->examStudent != null ? ($detail->examStudent->is_active ? 'bg-red-100' : '' ): '' ) }}">
                            @if($question->input_type == 'radio' )
                                
                                <div class="flex items-center h-5 mx-1">
                                <input type="{{$question->input_type}}"
                                value="{{$detail->detail}}"
                                name="{{$question->id}}"
                                {{$detail->examStudent->is_active ? 'checked':''}}>
                                </div>
                                <label class="mx-2">
                                {{$detail->detail}}                       
                                </label>                               
                            @elseif($question->input_type == 'checkbox')                        
                                <div class="flex items-center h-5 mx-1">
                                <input 
                                type="{{$question->input_type}}"
                                value="{{$detail->detail}}"
                                name="{{$question->id}}[]"
                                {{$detail->examStudent != null ?($detail->examStudent->is_active ? 'checked':''):''}}>
                                </div>
                                <label class="mx-2">
                                {{$detail->detail}}                       
                                </label>
                            @elseif($question->input_type == 'text')
                                <div class="flex items-center h-5 mx-1">
                                <input type="{{$question->input_type}}" 
                                class="w-full my-2 px-3 py-1 border rounded-lg text-sm mb-2 text-gray-700 focus:outline-none"
                                name="{{$question->id}}"
                                value="{{$detail->examStudent->is_active ? $detail->examStudent->detail : '' }}"
                                >
                                </div>
                                <label class="mx-2">
                                 Respuesta Correcta: {{$detail->detail}}    
                                </label>  
                            @elseif($question->input_type == 'textArea')   
                                <textarea name="{{$question->id}}"
                                class="w-full px-3 py-2 text-gray-700 border rounded-lg focus:outline-none" 
                                rows="3">{{$detail->examStudent->is_active ? $detail->examStudent->detail : '' }}</textarea>
                                <label class="mx-2">
                                    Respuesta Correcta: {{$detail->detail}}    
                                   </label> 
                            @endif
                    </div>  
                    @endforeach   
                    <div class="bg-gray-200 mt-2 rounded-md ">
                        <label class="flex justify-end mx-5 p-1">{{$suma}} / {{$question->details->sum('score')}}
                        </label>
                    </div>                   
                </div>                
        </div> 
        @endforeach   
    </div>
</div>
@endsection