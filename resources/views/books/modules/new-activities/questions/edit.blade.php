<form action="{{route('books.question.update', $question)}}" method="POST" class="space-y-8" enctype="multipart/form-data">
    @csrf
    <div class="mt-1 text-center sm:mt-1">
        <div class="px-1 py-1 border-b border-gray-200 sm:px-6">
            <h3 class="text-ml leading-6 font-medium text-gray-900">
                Nueva Pregunta
            </h3>
        </div>
        <div class="mt-6 grid row-gap-5 col-gap-4 grid-cols-1 sm:grid-cols-2" x-data="{typeContent : null}">
            <div class="col-span-2 text-left">
                <label for="name" class="block text-sm font-medium leading-5 text-gray-700">
                    Nombre de la pregunta
                </label>
                <div class="mt-1 rounded-md shadow-sm">
                    <input 
                        id="name" 
                        placeholder="Nombre de la actividad" 
                        name="name" 
                        value="{{old('name' , $question->name)}}"
                        type="text"
                        class="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                </div>
                <x-ui.error :errors="$errors" type="name" />
            </div>
            <div>
                <label for="document" class="block text-sm font-medium leading-5 text-gray-700">
                  Tipo de pregunta
                </label>
                <div       
                    x-model="typeContent"                   
                    name="input_type"
                    class="mt-1 relative rounded-md shadow-sm">
                    <select name="input_type" id="input_type" class="form-select text-sm block w-full">
                        <option value="">Escoje un tipo</option>
                        <option value="text" {{old('input_type',$question->input_type) == "text" ? 'selected' : '' }}>Texto</option>
                        <option value="textArea" {{old('input_type',$question->input_type) == "textArea" ? 'selected' : '' }}>Textarea</option>
                        <option value="radio" {{old('input_type',$question->input_type) == "radio" ? 'selected' : '' }}>Radio</option>
                        <option value="checkbox" {{old('input_type',$question->input_type) == "checkbox" ? 'selected' : '' }}>Check</option>
                    </select>
                </div>
                <x-ui.error :errors="$errors" type="input_type" />
            </div>
            <div class="col-span-2">           
                @foreach ($question->details as $item)               
                
                <div class="relative flex p-2 border rounded-tl-md rounded-tr-md sm:grid-cols-3">
                    <div class="flex items-center h-5">
                        <input 
                        {{$item->is_correct == true ? 'checked': ''}}
                        type="checkbox"
                        class="w-4 h-4 text-indigo-600 border-gray-300 cursor-pointer focus:ring-indigo-500" >
                    </div>
                    <input type="text"  value="{{$item->detail}}" readonly class="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-4 mx-2 "/>
                    <input type="number" value="{{$item->score}}" readonly class="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-4 mx-2 "/>
                </div>
                @endforeach
            </div>        
            <div class="col-span-2 flex justify-end">                
                <div class="ml-3 rounded-md shadow-sm">
                    
                    <input  type="button" onclick="document.getElementById('newQuestionModal').close()"
                        class="px-3 py-2 border border-gray-300 rounded-md text-sm leading-4 font-medium text-gray-700 bg-white hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out" value="Cancelar">
                        
                </div>
                <div class="ml-3 rounded-md shadow-sm">
                    <button type="submit"
                        class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-white bg-teal-600 hover:bg-teal-500 focus:outline-none focus:border-teal-700 focus:shadow-outline-teal active:bg-teal-700 transition ease-in-out duration-150">
                        Guardar
                    </button>
                </div>
            </div>             
        </div>
    </div>
</form>