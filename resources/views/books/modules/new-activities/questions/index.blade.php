<div class="lg:block mb-8 sticky py-6 bg-white px-8" >
    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
          <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
            <table class="min-w-full divide-y divide-gray-200">
              <thead class="bg-gray-50">
                <tr>
                  <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Nombre
                  </th>
                  <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Tipo
                  </th>
                  <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Puntos
                  </th>
                  <th scope="col" class="relative px-6 py-3">
                    <span class="sr-only">Edit</span>
                  </th>
                </tr>
              </thead>
              <tbody class="bg-white divide-y divide-gray-200">
                @foreach ($activitie->questions as $question)
                <tr>
                  <td class="px-6 py-4 whitespace-nowrap">
                    <div class="flex items-center">
                      <div class="ml-4">
                        <div class="text-sm font-medium text-gray-900">
                        <a href="javascript:void(0);" onclick="editQuestion({{ $question->id}})">{{ $question->name}}</a>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td class="px-6 py-4 whitespace-nowrap">
                    <div class="text-sm text-gray-500">{{ $question->input_type}}</div>
                  </td>
                  <td class="px-6 py-4 whitespace-nowrap">
                    <div class="text-sm text-gray-500">{{$question->details->sum('score')}}</div>
                  </td>
                  <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                   <!-- <a href="{{route('books.newActivitie.show', $question)}}"
                    class="text-indigo-600 hover:text-indigo-900">Edit</a>-->
                    <form action="{{route('books.question.destroy',$question)}}" method="post">
                        <input type="hidden" name="_method" value="delete">
                        @csrf
                        <button
                            class="text-red-600 hover:text-red-900"
                            type="submit">
                            Eliminar
                        </button>
                    </form>
                  </td>
                </tr>  
                @endforeach      
                <!-- More items... -->
              </tbody>
            </table>
          </div>
        </div>
      </div>     
     
</div>
<script>
  function editQuestion($id){  
    var URLactual = window.location.origin;
    var ruta = "/books/new-activitie/question/"+$id+"/edit"
           $.ajax({
              type: 'GET',
              url: URLactual+ruta,
              success: function (result, status, xhr) {
                $('#newQuestionModalInfo').html(result)
                document.getElementById('newQuestionModal').showModal();
              },
              error: function (xhr, status, error) {
              }
          });
  }
</script>
