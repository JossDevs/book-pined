@extends('layouts.books-show')
@section('content')
@php
    $breadcrumbs = collect([
        [
            'title' => 'Mis libros',
            'url' => route('books.index'),
        ],
        [
            'title' => $book->present()->name(),
            'url' => route('books.show', $book),
        ],
        [
            'title' => $module->present()->name(),
            'url' => route('books.modules.index', [$book]),
        ]
    ]);
@endphp
<x-ui.flash />
<div class="flex">
    @include('components.menu-left')   

    <div class="hidden lg:block lg:w-10"></div>
    <div class="bg-white shadow w-full sm:rounded-lg relative">
        <div class="px-4 py-5 border-b border-gray-200 sm:px-6">
            <h3 class="text-lg leading-6 font-medium text-gray-900">
                Actividad
            </h3>
        </div>
        <div class="lg:block mb-8 sticky py-6 bg-white px-8" >
            @include('books.modules.new-activities.index')
            <form action="{{route('books.newActivitie.store', $module)}}" method="POST" class="space-y-8" enctype="multipart/form-data">
                @csrf
                <div class="mt-6 grid row-gap-6 col-gap-4 grid-cols-1 sm:grid-cols-2" x-data="{typeContent : null}">
                    <div>
                        <label for="name" class="block text-sm font-medium leading-5 text-gray-700">
                            Nombre de la actividad.
                        </label>
                        <div class="mt-1 rounded-md shadow-sm">
                            <input 
                                id="name" 
                                placeholder="Nombre de la actividad" 
                                name="name" 
                                value="{{old('name')}}"
                                type="text"
                                class="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                        </div>
                        <x-ui.error :errors="$errors" type="name" />
                    </div>
                    <div>
                        <label for="name" class="block text-sm font-medium leading-5 text-gray-700">
                            &nbsp;
                        </label>
                    <div class="flex justify-end">
                        <div class="inline-flex rounded-md shadow-sm">
                            <a href="{{route('books.modules.index', [$book])}}"
                                class="py-2 px-4 border border-gray-300 rounded-md text-sm leading-5 font-medium text-gray-700 bg-white hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out">
                                Cancelar
                            </a>
                        </div>
                        <div class="ml-3 inline-flex rounded-md shadow-sm">
                            <button 
                            onClick="this.form.submit(); this.disabled=true;"
                                class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-white bg-teal-600 hover:bg-teal-500 focus:outline-none focus:border-teal-700 focus:shadow-outline-teal active:bg-teal-700 transition ease-in-out duration-150">
                                Guardar
                            </button>
                        </div>
                    </div> 
                    </div>                  
                </div>
            </form>
        </div>
    </div>
</div>
@endsection




