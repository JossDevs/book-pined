<div class="lg:block mb-8 sticky py-6 bg-white px-8" >
    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
          <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
            <table class="min-w-full divide-y divide-gray-200">
              <thead class="bg-gray-50">
                <tr>
                  <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Nombre
                  </th>
                  <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Creada
                  </th>
                  <th scope="col" class="relative px-6 py-3">
                    <span class="sr-only">Edit</span>
                  </th>
                </tr>
              </thead>
              <tbody class="bg-white divide-y divide-gray-200">
                @foreach ($module->newActivities->sortBydesc('id') as $newActivitie)
                <tr>
                  <td class="px-6 py-4 whitespace-nowrap">
                    <div class="flex items-center">
                      <div class="ml-4">
                        <div class="text-sm font-medium text-gray-900">
                            {{ $newActivitie->name}}
                        </div>
                      </div>
                    </div>
                  </td>
                  <td class="px-6 py-4 whitespace-nowrap">
                    <div class="text-sm text-gray-500">27-12-1987</div>
                  </td>
                  <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                    <a href="{{route('books.newActivitie.show', $newActivitie)}}"
                    class="text-indigo-600 hover:text-indigo-900">Edit</a>
                    <form action="{{route('books.newActivitie.destroy', $newActivitie)}}" method="post">
                        <input type="hidden" name="_method" value="delete">
                        @csrf
                        <button
                            class="text-red-600 hover:text-red-900"
                            type="submit">
                            Eliminar
                        </button>
                    </form>
                  </td>
                </tr>  
                @endforeach  
                <!-- More items... -->
              </tbody>
            </table>
          </div>
        </div>
      </div>
</div>


