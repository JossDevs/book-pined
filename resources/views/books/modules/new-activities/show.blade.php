@extends('layouts.books-show')
@section('content')
@php
    $breadcrumbs = collect([
        [
            'title' => 'Mis libros',
            'url' => route('books.index'),
        ],
        [
            'title' => $book->present()->name(),
            'url' => route('books.show', $book),
        ],
        [
            'title' => $module->present()->name(),
            'url' => route('books.modules.index', [$book]),
        ]
    ]);
@endphp
<x-ui.flash />
<div class="flex">

    @include('components.menu-left')   

    <div class="hidden lg:block lg:w-10"></div>    
    <div class="bg-white shadow w-full sm:rounded-lg relative">
        <div class="text-right m-2">
            @role('admin')
                <a href="#" onclick="nuevaPregunta()" type="button" class="inline-flex items-center px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:shadow-outline-indigo focus:border-indigo-700 active:bg-indigo-700 transition duration-150 ease-in-out">
                    Agregar Pregunta
                </a>
            @endrole
        </div>
        <div class="px-4 py-5 border-b border-gray-200 sm:px-6">
            <h3 class="text-lg leading-6 font-medium text-gray-900">
               Actividad: {{$newActivitie->name}}
            </h3>
        </div>
        @include('books.modules.new-activities.questions.index',[
            'activitie' => $newActivitie
        ])        
    </div>
</div>
<dialog id="newQuestionModal" class="z-50 w-11/12 mx-auto overflow-y-auto bg-white rounded shadow-lg modal-container md:max-w-xl ">
    <div id='newQuestionModalInfo'>

    </div>

</dialog>
<script type="text/javascript">
    function nuevaPregunta()
    { var ruta = "{{route('books.question.create', $newActivitie)}};"
           $.ajax({
              type: 'GET',
              url: ruta,
              success: function (result, status, xhr) {
                $('#newQuestionModalInfo').html(result)
                document.getElementById('newQuestionModal').showModal();
              },
              error: function (xhr, status, error) {
              }
          });
    }
   </script>
@endsection