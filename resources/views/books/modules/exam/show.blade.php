@extends('layouts.books-show')
@section('content')
@php
    $breadcrumbs = collect([
        [
            'title' => 'Mis libros',
            'url' => route('books.index'),
        ],
        [
            'title' => $book->present()->name(),
            'url' => route('books.show', $book),
        ],
        [
            'title' => $module->present()->name(),
            'url' => route('books.modules.index', [$book]),
        ]
    ]);
@endphp
<x-ui.flash />
<div class="flex">
    @include('components.menu-left')   

    <div class="hidden lg:block lg:w-10"></div>
    <div class="bg-white shadow w-full sm:rounded-lg relative">
        <div class="px-4 py-5 border-b border-gray-200 sm:px-6">
            <h3 class="text-lg leading-6 font-medium text-gray-900">
                Actividad: {{$newActivitie->name}}
            </h3>
        </div>
        @foreach ($questions as $key=>$question)
        <div class="shadow px-6 py-4 bg-blue-50 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
            <div class="flex justify-between">
                <div class="space-x-2 flex items-center">
                    <img class="h-8 w-8 rounded-full" src="https://ui-avatars.com/api/?name={{$loop->iteration}}"
                        alt="{{$loop->iteration}}">
                    <div>
                            {{$question->name}}
                    </div>
                </div>
                <div>
                </div>
            </div>
        </div>
        
        <div class="shadow px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
            <form action="{{route('books.exam.store',$newActivitie )}}" method="post">
                @csrf
                <div class=" justify-between">                    
                    @foreach ($question->details as $detail)
                    <div class="relative flex p-2 border rounded-tl-md rounded-tr-md mt-1">                    
                            @if($question->input_type == 'radio' )                        
                                <div class="flex items-center h-5 mx-1">
                                <input type="{{$question->input_type}}"
                                value="{{$detail->detail}}"
                                name="{{$question->id}}">
                                </div>
                                <label class="mx-2">
                                {{$detail->detail}}                       
                                </label>
                            @elseif($question->input_type == 'checkbox')                        
                                <div class="flex items-center h-5 mx-1">
                                <input type="{{$question->input_type}}"
                                value="{{$detail->detail}}"
                                name="{{$question->id}}[]">
                                </div>
                                <label class="mx-2">
                                {{$detail->detail}}                       
                                </label>
                            
                            @elseif($question->input_type == 'text')
                                <div class="flex items-center h-5 mx-1">
                                <input type="{{$question->input_type}}" 
                                class="w-full my-2 px-3 py-1 border rounded-lg text-sm mb-2 text-gray-700 focus:outline-none"
                                name="{{$question->id}}">
                                </div>
                                <label class="mx-2">
                                Escriba su respuesta    
                                </label>  
                            @elseif($question->input_type == 'textArea')   
                                <textarea name="{{$question->id}}"
                                class="w-full px-3 py-2 text-gray-700 border rounded-lg focus:outline-none" 
                                rows="3"></textarea>
                                <label class="mx-2">
                                Escriba su respuesta    
                                </label>  
                            @endif
                    </div>         
                    @endforeach                    
                </div>                
        </div> 
        @endforeach   
        <div class="border-t border-gray-200 pt-5">
            <div class="flex justify-end">
                <div class="ml-3 inline-flex rounded-md shadow-sm m-3">
                    <button type="submit"
                        class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-white bg-teal-600 hover:bg-teal-500 focus:outline-none focus:border-teal-700 focus:shadow-outline-teal active:bg-teal-700 transition ease-in-out duration-150">
                        Guardar
                    </button>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
@endsection