@extends('layouts.books-show')
@section('content')
@php
    $breadcrumbs = collect([
        [
            'title' => 'Mis libros',
            'url' => route('books.index'),
        ],
        [
            'title' => 'Agregar Libro',
            'url' => '#'
        ]
    ]);
@endphp
<form action="{{route('books.store')}}" method="POST" class="space-y-8" enctype="multipart/form-data">
    @csrf
    <div class="mt-6 grid row-gap-6 col-gap-4 grid-cols-1 sm:grid-cols-2">
        <div>
            <label for="title" class="block text-sm font-medium leading-5 text-gray-700">
                Nombre del libro.
            </label>
            <div class="mt-1 rounded-md shadow-sm">
                <input 
                    id="title" 
                    placeholder="Título" 
                    name="name" 
                    type="text"
                    value="{{old('name')}}"
                    class="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5">
            </div>
            <x-ui.error :errors="$errors" type="name" />
        </div>
        <div>
            <label for="document" class="block text-sm font-medium leading-5 text-gray-700">
                Portada
            </label>
            <div class="mt-1 relative rounded-md shadow-sm">
                <input 
                    id="document" 
                    name="cover" 
                    type="file"
                    accept="image/jpeg"
                    class="form-input block w-full sm:text-sm sm:leading-5" style="padding: 0.35rem 0.5rem;">
            </div>
            <x-ui.error :errors="$errors" type="cover" />
        </div>
    </div>
    <div class="border-t border-gray-200 pt-5">
        <div class="flex justify-end">
            <div class="inline-flex rounded-md shadow-sm">
                <a href="{{route('books.index')}}"
                    class="py-2 px-4 border border-gray-300 rounded-md text-sm leading-5 font-medium text-gray-700 bg-white hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out">
                    Cancelar
                </a>
            </div>
            <div class="ml-3 inline-flex rounded-md shadow-sm">
                <button 
                    type="submit"
                    class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-white bg-teal-600 hover:bg-teal-500 focus:outline-none focus:border-teal-700 focus:shadow-outline-teal active:bg-teal-700 transition ease-in-out duration-150">
                    Guardar
                </button>
            </div>
        </div>
    </div>
</form>
@endsection