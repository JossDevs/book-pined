@extends('layouts.app')

@section('template_title')
    {{ $student->name ?? 'Mostrar estudiante' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Estudiantes</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('students.index') }}"> Atrás</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Nombres:</strong>
                            {{ $student->first_name }}
                        </div>
                        <div class="form-group">
                            <strong>Apellidos:</strong>
                            {{ $student->last_name }}
                        </div>
                        <div class="form-group">
                            <strong>Identificacion:</strong>
                            {{ $student->identification_number }}
                        </div>
                        <div class="form-group">
                            <strong>Email:</strong>
                            {{ $student->email }}
                        </div>                   

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
