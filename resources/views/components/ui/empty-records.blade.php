@props(['count'])
@if($count == 0)
    <p class="p-6 text-center text-gray-500 text-sm">
        {{ $slot }}
    </p>
@endif
