@php
    $main_menu = collect([
        [
            'title' => 'Mis Libros',
            'url' => route('books.index'),
            'is_active' => request()->routeIs('books.*'),
            'icon' => '<i class="mr-2 fal fa-books"></i>'
        ],
    ]);
    $menu_desktop_active_class = 'group flex items-center px-2 py-2 text-base leading-6 font-medium rounded-md text-white bg-gray-900 focus:outline-none focus:bg-gray-700 transition ease-in-out duration-150';
    $menu_desktop_inactive_class = 'group flex items-center px-2 py-2 text-base leading-6 font-medium rounded-md text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150';

    $menu_movil_active_class = 'group flex items-center px-2 py-2 text-sm leading-5 font-medium text-white rounded-md bg-gray-900 focus:outline-none focus:bg-gray-700 transition ease-in-out duration-150';
    $menu_movil_inactive_class = 'group flex items-center px-2 py-2 text-sm leading-5 font-medium text-gray-300 rounded-md hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150';
@endphp


<!-- Off-canvas menu for mobile -->
<div x-show="sidebarOpen" class="md:hidden" style="display: none;">
    <div class="fixed inset-0 flex z-40">
        <div @click="sidebarOpen = false" x-show="sidebarOpen"
            x-description="Off-canvas menu overlay, show/hide based on off-canvas menu state."
            x-transition:enter="transition-opacity ease-linear duration-300" x-transition:enter-start="opacity-0"
            x-transition:enter-end="opacity-100" x-transition:leave="transition-opacity ease-linear duration-300"
            x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" class="fixed inset-0">
            <div class="absolute inset-0 bg-gray-600 opacity-75"></div>
        </div>
        <div x-show="sidebarOpen" x-description="Off-canvas menu, show/hide based on off-canvas menu state."
            x-transition:enter="transition ease-in-out duration-300 transform"
            x-transition:enter-start="-translate-x-full" x-transition:enter-end="translate-x-0"
            x-transition:leave="transition ease-in-out duration-300 transform"
            x-transition:leave-start="translate-x-0" x-transition:leave-end="-translate-x-full"
            class="relative flex-1 flex flex-col max-w-xs w-full bg-gray-800" style="display: none;">
            <div class="absolute top-0 right-0 -mr-14 p-1">
                <button x-show="sidebarOpen" @click="sidebarOpen = false"
                    class="flex items-center justify-center h-12 w-12 rounded-full focus:outline-none focus:bg-gray-600"
                    aria-label="Close sidebar" style="display: none;">
                    <svg class="h-6 w-6 text-white" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M6 18L18 6M6 6l12 12"></path>
                    </svg>
                </button>
            </div>
            <div class="flex-1 h-0 pt-5 pb-4 overflow-y-auto">
                <div class="flex-shrink-0 flex items-center justify-center text-center px-4">
                    <img class="h-8 w-auto" src="{{asset('img/logo-hemafre-texto.png')}}" alt="Hemafre Ediciones">
                </div>
                <nav class="mt-5 px-2">
                    @foreach ($main_menu as $menu)
                        <a href="{{$menu['url']}}"
                            class="{{$menu['is_active'] ? $menu_desktop_active_class : $menu_desktop_inactive_class}}">
                            {!!$menu['icon']!!}
                            {{$menu['title']}}
                        </a>
                    @endforeach
                </nav>
            </div>
            <div class="flex-shrink-0 flex bg-gray-700 p-4">
                <a href="#" class="flex-shrink-0 group block">
                    <div class="flex items-center">
                        <div>
                            <img class="inline-block h-10 w-10 rounded-full"
                                src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=facearea&amp;facepad=2&amp;w=256&amp;h=256&amp;q=80"
                                alt="">
                        </div>
                        <div class="ml-3">
                            <p class="text-base leading-6 font-medium text-white">
                                Tom Cook
                            </p>
                            <p
                                class="text-sm leading-5 font-medium text-gray-400 group-hover:text-gray-300 transition ease-in-out duration-150">
                                View profile
                            </p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="flex-shrink-0 w-14">
            <!-- Force sidebar to shrink to fit close icon -->
        </div>
    </div>
</div>

<!-- Static sidebar for desktop -->
<div class="hidden md:flex md:flex-shrink-0">
    <div class="flex flex-col w-64 bg-gray-800">
        <div class="h-0 flex-1 flex flex-col pt-5 pb-4 overflow-y-auto">
            <div class="flex items-center justify-center flex-shrink-0 px-4">
                <img class="h-8 w-auto" src="{{asset('img/logo-hemafre-texto.png')}}" alt="Hemafre Ediciones">
            </div>
            <!-- Sidebar component, swap this element with another sidebar if you like -->
            <nav class="h-full flex flex-col justify-between">
                @foreach ($main_menu as $menu)
                    <li class="mt-5 flex-1 px-2 bg-gray-800 list-none" x-data="{showSubMenu : false}">
                        <a href="{{$menu['url']}}"
                            class="{{$menu['is_active'] ? $menu_movil_active_class : $menu_movil_inactive_class}} flex justify-between items-center">
                            <div>
                                {!!$menu['icon']!!}
                                {{$menu['title']}}
                            </div>
                        </a>
                    </li>
                @endforeach
                <p class="text-center">
                    <form action="{{route('logout')}}" method="post">
                        @csrf
                        <div class="flex justify-center items-center space-x-2 text-gray-300">
                            <i class="fas fa-sign-out-alt"></i>
                            <button
                                type="submit"
                                class="text-sm">
                                Cerrar Sesión
                            </button>
                        </div>
                    </form>
                </p>
            </nav>
        </div>
        <div class="flex-shrink-0 flex bg-gray-700 p-4">
            <a href="#" class="flex-shrink-0 w-full group block">
                <div class="flex items-center">
                    <div>
                        <img class="inline-block h-9 w-9 rounded-full"
                            src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=facearea&amp;facepad=2&amp;w=256&amp;h=256&amp;q=80"
                            alt="">
                    </div>
                    <div class="ml-3">
                        <p class="text-sm leading-5 font-medium text-white">
                            {{--auth()->user()->first_name--}}
                        </p>
                        <p
                            class="text-xs leading-4 font-medium text-gray-300 group-hover:text-gray-200 transition ease-in-out duration-150">
                            Editar Perfil
                           
                        </p>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>