<div class="hidden bg-white self-start shadow sm:rounded-lg w-72 sticky top-0 lg:block">
    <div class="px-4 py-5 border-b border-gray-200 sm:px-6">
        <h3 class="text-lg leading-6 font-medium text-gray-900">
            Modulos
        </h3>
    </div>
    <div class="px-3 py-5 sm:p-0">
        <dl>
            <div class="sm:grid">
                @foreach ($book->modules as $module)
                    <div class="{{$loop->index > 0 ? 'sm:border-t sm:border-gray-200' : ''}} flex justify-between items-center mt-1 text-xs py-3 px-3 text-gray-900 sm:mt-0 hover:bg-gray-100 {{request('page') == $loop->index + 1 ? 'bg-gray-100' : ''}}">
                        <a href="{{route('books.modules.index', [$book, 'page' => $loop->index+1])}}" class="">
                            {{$module->present()->name()}}
                        </a>                            
                          <div x-data="{ dropdownOpen{{$module->id}}: false }" class=" align-middle">
                            <button @click="dropdownOpen{{$module->id}} = !dropdownOpen{{$module->id}}" class="relative z-10 block bg-gray-50 rounded p-2 hover:bg-gray-200 focus:outline-none focus:bg-gray-300">
                              <svg class="h-6 w-6 text-black" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z" />
                              </svg>
                            </button>
                        
                            <div x-show="dropdownOpen{{$module->id}}" @click="dropdownOpen{{$module->id}} = false" class="fixed inset-0 h-full w-full z-10"></div>
                            
                            <div x-show="dropdownOpen{{$module->id}}" class="absolute left-0 mt-2 w-48 bg-white rounded-md overflow-hidden shadow-xl z-20">
                              @role('admin')
                              <a href="{{route('books.newActivitie.create', $module)}}" class="block px-4 py-2 text-sm text-gray-800 border-b hover:bg-gray-300 bg-gray-200">Nueva Actividad</a>
                              @foreach ($module->newActivities as $newActivitie)
                              <a href="{{route('books.newActivitie.show', $newActivitie)}}" class="block px-4 py-2 text-sm text-gray-800 border-b hover:bg-gray-200">{{$newActivitie->name}} </a> 
                              @endforeach  
                              @endrole 
                              @role('student')
                              @foreach ($module->newActivities as $newActivitie)
                              <a href="{{route('books.exam.show', $newActivitie)}}" class="block px-4 py-2 text-sm text-gray-800 border-b hover:bg-gray-200">{{$newActivitie->name}} </a> 
                              @endforeach  
                              @endrole                             
                            </div>                            
                          </div>
                        </div>
                @endforeach
            </div>
        </dl>
    </div>
</div>