<main class="flex-1 relative z-0 overflow-y-auto pb-6 focus:outline-none" tabindex="0" x-data=""
    x-init="$el.focus()">
    @yield('section-title')
    <div class="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
        <!-- Replace with your content -->
        <div class="py-4">
            {{$slot}}
        </div>
        <!-- /End replace -->
    </div>
</main>