<div>
    <div class="max-w-xl mx-auto space-y-4">
        <nav role="navigation" aria-label="Pagination Navigation" class="flex justify-between">
            <button rel="prev"
                wire:click="previousImage"
                class="{{$this->index == 0 ? $buttonClassInactive : $buttonClassActive}}">
                « Anterior
            </button>
    
            <button
                wire:click="nextImage"
                class="{{$module->images()->count() === $this->index + 1 ? $buttonClassInactive : $buttonClassActive}}">
                Siguiente »
            </button>
        </nav>
        <figure>
            @foreach ($module->images() as $image)
                @if ($this->index === $loop->index)
                    <img src="{{$image->present()->getUrl()}}" alt="{{$image->present()->getUrl()}}">
                @endif
            @endforeach
        </figure>
    </div>
</div>