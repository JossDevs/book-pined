<div>
    <p class="text-center leading-12 font-extrabold text-gray-900 lg:text-3xl">The verb <b>'To Be'</b></p>
    
    <p class="text-center leading-12 font-bold text-gray-900 lg:Text-xl">The verb 'to be'(affirmative)</p>
    
    <div class="h-6"></div>
    
    <div class="mx-auto max-w-xl">
        <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
            <div class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                <table class="min-w-full">
                    <thead>
                        <tr class="bg-blue-600 text-white">
                            <th height="50" colspan="2">
                                AFFIRMATIVE
                            </th>
                        </tr>
                        <tr>
                            <th
                                class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Full Form
                            </th>
                            <th
                                class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Short Form
                            </th>
                        </tr>
                    </thead>
                    <tbody class="bg-white">
                        <tr>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                                I <b>am</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                I<b>'m</b>
                            </td>
                        </tr>
                        <tr>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                                you <b>are</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                you<b>'re</b>
                            </td>
                        </tr>
                        <tr>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                                he <b>is</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                he<b>'s</b>
                            </td>
                        </tr>
                        <tr>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                                she <b>is</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                she<b>'s</b>
                            </td>
                        </tr>
                        <tr>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                                it <b>is</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                it<b>'s</b>
                            </td>
                        </tr>
                        <tr>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                                we <b>are</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                we<b>'re</b>
                            </td>
                        </tr>
                        <tr>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                                you <b>are</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                you<b>'re</b>
                            </td>
                        </tr>
                        <tr>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                                they <b>are</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                the<b>'re</b>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    <div class="h-6"></div>
    
    <p>We usually use the <b>full form</b> of the verb <b>to be</b> in written English. <i>They <b>are</b> from Canada and they <b>are</b> seventeen years old.</i></p>
    
    <p>We use the <b>contracted form</b> of the verb <b>to be</b> in spoken English and informal written English. </p>
    
    <p>A: <i>What's your name?</i></p>
    
    <p>B: <i>My name's Marco and I'm from Italy</i></p>
    
    <div class="h-8"></div>
    
    <div class="flex justify-center">
        <img class="rounded shadow w-72" src="https://images.unsplash.com/photo-1543269865-4430f94492b9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80" alt="">
    </div>
    
    <div class="h-4"></div>
    
    <p class="text-blue-700 text-xl text-center italic"><b>He's</b> James and <b>she's</b> Sue.</p>
    
    <p class="text-blue-700 text-xl text-center italic"> <b>They are</b> sixteen years old. </p>
    
    <h1 class="border-t border-gray-200 mt-10 mb-10"></h1>
    
    <p class="tobe2 text-center leading-12 font-extrabold text-gray-900 lg:text-3xl">The verb <b>'To Be'</b> (negative & questions)</p>
    
    <div class="h-4"></div>
    
    <div id="testing" class="mx-auto max-w-xl">
        <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
            <div class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                <table class="min-w-full">
                    <thead>
                        <tr class="bg-blue-600 text-white">
                            <th colspan="2" height="50" colspan="2">
                                NEGATIVE
                            </th>
                            <th height="50" rowspan="2">
                                QUESTIONS
                            </th>
                        </tr>
                        <tr>
                            <th
                                class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Full Form
                            </th>
                            <th
                                class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Short Form
                            </th>
                        </tr>
                    </thead>
                    <tbody class="bg-white">
                        <tr>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                                I <b>am not</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                I<b>'m not</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                <b>Am</b> I?
                            </td>
                        </tr>
                        <tr>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                                you <b>are not</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                you <b>'re not /</b> you <b>aren't</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                <b>Are</b> you?
                            </td>
                        </tr>
                        <tr>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                                He <b>is not</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                he<b>'s not /</b> he <b>isn't</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                <b>Is</b> he?
                            </td>
                        </tr>
                        <tr>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                                she <b>is not</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                she<b>'s not /</b> she <b>isn't</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                <b>Is</b> she?
                            </td>
                        </tr>
                        <tr>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                                it <b>is not</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                it<b>'s not /</b> it <b>isn't</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                <b>Is</b> it?
                            </td>
                        </tr>
                        <tr>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                                we <b>are not</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                we<b>'re not /</b> we <b>aren't</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                <b>Are</b> we?
                            </td>
                        </tr>
                        <tr>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                                you <b>are not</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                you<b>'re not /</b> you <b>aren't</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                <b>Are</b> you?
                            </td>
                        </tr>
                        <tr>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                                they <b>are not</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                they<b>'re not /</b> they <b>aren't</b>
                            </td>
                            <td
                                class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                <b>Are</b> they?
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    <div class="h-8"></div>
    
    <p> In short answers, we do not repeat the whole question. We use <b>Yes or No,</b> the subject pronoun, and the appropriate verb form. </p>
    
    <p> We use the full form of the verb <b>to be</b> in <b>positive short answers. </b> <i>Are you a teacher? Yes, <b>I am.</b> </i> (NOT: <i class="line-through"> Yes, I'm</i>) </p>
    
    <p>We use the <b>contracted form</b> of the verb <b>to be</b> in <b>negative short answers.</b> </p>
    
    <p> <i>Are you French? No, <b>I'm not.</b></i> (NOT: <i class="line-through">No, i am not </i> ) </p>
    
    <div class="h-4"></div>
    
    <div class="flex justify-center">
        <img class="rounded shadow w-72" src="https://images.unsplash.com/photo-1487412720507-e7ab37603c6f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1351&q=80" alt="">
    </div>
    
    <div class="h-4"></div>
    
    <p class="text-blue-700 text-xl text-center italic"><b>Are you</b> from Italy.</p>
    
    <p class="text-blue-700 text-xl text-center italic"> No, <b>I'm not. I'm</b> from Mexico.</p>
</div>