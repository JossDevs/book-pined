@extends('layouts.app')
@section('content')
    @section('section-title')
        <x-ui.page-header
            :breadcrumbs="$breadcrumbs">

            {{$titulo}}
        </x-ui.page-header>
    @endsection
    @yield('calificaciones-content')
@endsection