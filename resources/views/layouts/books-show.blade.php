@extends('layouts.app')
@section('content')
    @section('section-title')
        <x-ui.page-header
            :breadcrumbs="$breadcrumbs">
            Mis Libros
        </x-ui.page-header>
    @endsection
    @yield('books-content')
@endsection