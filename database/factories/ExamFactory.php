<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Exam;
use App\QuestionDetail;
use App\Student;
use Faker\Generator as Faker;

$factory->define(Exam::class, function (Faker $faker) {
    return [

        'detail' =>$faker->sentence(2),
        'score' => $faker->numberBetween(01,99),
        'is_active' => true,
        'question_detail_id' =>  function () {
            return factory(QuestionDetail::class)->create();
        },
        'student_id' =>  function () {
            return factory(Student::class)->create();
        },
    ];
});