<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Book;
use App\Module;
use Faker\Generator as Faker;

$factory->define(Module::class, function (Faker $faker) {

    $content = "<h3 class='text-center leading-12 font-extrabold text-gray-900 lg:text-3xl mb-4'>{$faker->sentence(6)}</h3><p class='mb-4'>{$faker->sentence(rand(20,60))}</p> <p class='mb-4'>{$faker->sentence(rand(20,60))}</p> <p class='mb-4'>{$faker->sentence(rand(20,60))}</p> <p class='mb-4'>{$faker->sentence(rand(20,60))}</p><h3 class='text-center leading-12 font-extrabold text-gray-900 lg:text-xl mb-4'>{$faker->sentence(6)}</h3> <p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><h3 class='text-center leading-12 font-extrabold text-gray-900 lg:text-xl mb-4'>{$faker->sentence(6)}</h3> <p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><h3 class='text-center leading-12 font-extrabold text-gray-900 lg:text-xl mb-4'>{$faker->sentence(6)}</h3> <p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><h3 class='text-center leading-12 font-extrabold text-gray-900 lg:text-xl mb-4'>{$faker->sentence(6)}</h3> <p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><h3 class='text-center leading-12 font-extrabold text-gray-900 lg:text-xl mb-4'>{$faker->sentence(6)}</h3> <p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p><p class='mb-4'>{$faker->sentence(rand(20,60))}</p>";

    return [
        'name' => $faker->sentence(2),
        'body' => "$content",
        'type' => 'html',
        'book_id' =>  function () {
            return factory(Book::class)->create();
        },
    ];
});
