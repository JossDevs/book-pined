<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ActiviyQuestion;
use App\QuestionDetail;
use Faker\Generator as Faker;

$factory->define(QuestionDetail::class, function (Faker $faker) {
    return [
        'detail' => $faker->sentence(2),
        'is_correct' => true,
        'score' => $faker->numberBetween(01,99),
        'activity_question_id' =>  function () {
            return factory(ActiviyQuestion::class)->create();
        },
    ];
});
