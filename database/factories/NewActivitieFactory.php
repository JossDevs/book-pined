<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\NewActivitie;
use App\Module;
use App\User;
use Faker\Generator as Faker;

$factory->define(NewActivitie::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(2),
        'module_id' =>  function () {
            return factory(Module::class)->create();
        },
        'user_id' =>  function () {
            return factory(User::class)->create();
        },
        
    ];
});
