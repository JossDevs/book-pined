<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ActiviyQuestion;
use App\InputType;
use App\NewActivitie;
use Faker\Generator as Faker;

$factory->define(ActiviyQuestion::class, function (Faker $faker) {
    
    return [

        'name' => $faker->sentence(2),        
        'input_type' =>   $faker->sentence(1),
        'new_activitie_id' =>  function () {
            return factory(NewActivitie::class)->create();
        },
    ];
});