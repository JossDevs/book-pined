<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Module;
use App\Topic;
use Faker\Generator as Faker;

$factory->define(Topic::class, function (Faker $faker) {
    $name = $faker->sentence(2);
    return [
        'name' => $name,
        'anchor_text' => $name,
        'module_id' => function () {
            return factory(Module::class)->create();
        },
    ];
});
