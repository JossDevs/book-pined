<?php

use App\Student;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('students')->delete();
        // DB::table('users')->delete();

        $users = [
            // [
            //     'name' => 'LORENA SANLUCAS',
            //     'email' => 'lorenasabando@yahoo.es'
            // ],
            // [
            //     'name' => 'AZUCENA ABAD',
            //     'email' => 'azucenaabadgame@gmail.com'
            // ],
            // [
            //     'name' => 'INES ROJAS',
            //     'email' => 'ivetterojas_73@hotmail.com'
            // ],
            // [
            //     'name' => 'FERMIN BAJAÑA ROMAN',
            //     'email' => 'morenopiguavekatty@gmail.com'
            // ],
            // [
            //     'name' => 'VANESSA AGILA',
            //     'email' => 'agilavanessa@yahoo.com'
            // ],
            // [
            //     'name' => 'ANGIE BENAVIDES',
            //     'email' => 'advc_21@hotmail.com'
            // ],
            // [
            //     'name' => 'PAMELA LARREA',
            //     'email' => 'p_larrea@yahoo.com'
            // ],
            // [
            //     'name' => 'PABLO ORTIZ TELLO',
            //     'email' => 'portiztello1073@icloud.com'
            // ],
            // [
            //     'name' => 'GINGER RENDON',
            //     'email' => 'ginreja@hotmail.com'
            // ],
            // [
            //     'name' => 'MARTHA ALCIVAR',
            //     'email' => 'Roxana_alcivar@hotmail.com'
            // ],
            [
                'name' => 'Prueba Libro',
                'email' => 'soporte@pined.ec'
            ],
            // [
            //     'name' => 'EXPRESION ARTISTICA 2',
            //     'email' => 'expart2@pined.ec'
            // ],
            // [
            //     'name' => 'PAEL SIERRA',
            //     'email' => 'psierral59@hotmail.com'
            // ],
            // [
            //     'name' => 'SORAYA CORONEL',
            //     'email' => 'soryacoronel@hotmail.com'
            // ],
        ];

        // $admin = factory(User::class)->create([
        //     'email' => 'admin@gmail.com',
        //     'password' => bcrypt('edicioneshemafre')
        // ]);

        // $admin->assignRole('admin');

        foreach ($users as $user) {
            $name = explode(" ", $user['name']);

            $user = factory(Student::class)->create([
                'first_name' => $name[0],
                'last_name' => $name[1],
                'email' => $user['email'],
                'password' => bcrypt('password'),
                'user_id' => function ($student) {
                    return factory(User::class)->create([
                        'email' => $student['email'],
                        'password' => $student[('password')]
                    ]);
                },

            ]);

            $user->user->assignRole('student');
        }


    }
}
