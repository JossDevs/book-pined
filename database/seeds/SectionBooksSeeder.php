<?php

use App\Book;
use App\Module;
use App\Topic;
use Illuminate\Database\Seeder;

class SectionBooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $book = factory(Book::class)->create();

        factory(Module::class, rand(6,9))->create([
            'book_id' => $book->id
        ]);

    }
}
